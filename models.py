# Standard library imports
import datetime
from decimal import Decimal

# Third-party importss
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, UniqueConstraint
from sqlalchemy.orm import sessionmaker, relationship, exc

engine = create_engine('sqlite+pysqlite:///db.sqlite3')

Base = declarative_base(bind=engine)
Session = sessionmaker(bind=engine)


class Model(object):
    def to_dict(self):
        """Return the resource as a dictionary.

        :rtype: dict
        """
        result_dict = {}
        for column in self.__table__.columns.keys():  # pylint: disable=no-member
            value = result_dict[column] = getattr(self, column, None)
            if isinstance(value, Decimal):
                result_dict[column] = float(result_dict[column])
            elif isinstance(value, datetime.datetime):
                result_dict[column] = value.isoformat()
        return result_dict


class ServerProfile(Base, Model):
    __tablename__ = 'servers'
    __table_args__ = (
        UniqueConstraint("api_root", "application_id"),
    )

    id = Column(Integer, primary_key=True)
    api_root = Column(String, index=True, nullable=False)
    application_id = Column(String, index=True, nullable=False)
    rest_api_key = Column(String, nullable=False)
    master_key = Column(String)
    profiles = relationship('ImportProfile')


class ImportProfile(Base):
    __tablename__ = 'profiles'

    id = Column(Integer, primary_key=True)
    name = Column(String, index=True, nullable=False, unique=True)
    source_file = Column(String)
    source_code_page = Column(String)
    import_type = Column(String)
    import_mode = Column(String)
    use_extended_insert = Column(Boolean)
    is_fixed_width = Column(Boolean, default=False)
    field_delimiter = Column(String)
    row_delimiter = Column(String)
    text_qualifier = Column(String)
    first_row = Column(Integer)
    last_row = Column(Integer)
    field_name_row = Column(Integer)
    date_format = Column(String)
    date_separator = Column(String)
    time_separator = Column(String)
    decimal_separator = Column(String)
    date_time_tz_order = Column(String)
    use_empty_str_as_null = Column(Boolean)
    use_fkey_constraint = Column(Boolean)
    continue_on_error = Column(Boolean)
    include_unique_index_fkey = Column(Boolean)
    create_auto_inc = Column(Boolean)
    import_deleted_record = Column(Boolean)
    binary_data_encoding = Column(String)
    access_system_db_file = Column(String)
    access_db_password = Column(String)
    access_logon_name = Column(String)
    access_logon_password = Column(String)
    break_list = Column(String)
    watch_folder = Column(String)
    watch_file = Column(String)
    watch_file_regex = Column(Boolean)
    polling_interval = Column(Integer)
    server_id = Column(Integer, ForeignKey('servers.id'))
    enable = Column(Boolean, default=True)
    server = relationship("ServerProfile", back_populates="profiles")

    tables = relationship("Table")


class Table(Base, Model):
    __tablename__ = 'tables'

    id = Column(Integer, primary_key=True)
    source_schema_name = Column(String)
    source_table_name = Column(String)
    target_table_name = Column(String)
    is_new_table = Column(Boolean)
    condition_query = Column(String)
    query = Column(String)
    source_file = Column(String)
    profile_id = Column(Integer, ForeignKey('profiles.id'))
    fields = relationship("Field")


class Field(Base, Model):
    __tablename__ = 'fields'

    id = Column(Integer, primary_key=True)
    source_field_name = Column(String, nullable=False)
    source_field_create_name = Column(String, nullable=False)
    source_field_type = Column(String, default='TEXT')
    source_field_length = Column(Integer, default=0)
    source_field_scale = Column(Integer, default=0)
    source_is_selected = Column(Boolean, default=True)
    source_auto_inc = Column(Boolean, default=False)
    target_field_name = Column(String, nullable=False)
    target_field_type = Column(String, default='TEXT')
    target_field_length = Column(Integer, default=0)
    target_field_scale = Column(Integer, default=0)
    target_is_key = Column(Boolean, default=False)
    target_allow_null = Column(Boolean, default=True)
    target_is_auto_inc = Column(Boolean, default=False)
    table_id = Column(Integer, ForeignKey('tables.id'))


'''
class AppProfile(Base):
    __tablename__ = 'apps'

    id = Column(Integer, primary_key=True)
    name = Column(String, index=True, nullable=False, unique=True)
    folder = Column(String)
    file = Column(String)
    is_regex = Column(Boolean, default=False)
    delete_after_process = Column(Boolean, default=False)
    dump_path = Column(String)
    enable = Column(Boolean, default=True)
    template_id = Column(Integer, ForeignKey('profiles.id'))
'''

Base.metadata.create_all(engine)
