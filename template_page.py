from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QGroupBox, QFormLayout, QLabel, QVBoxLayout

import utils


class TemplatePage(QWidget):
    def __init__(self, title, parent=None):
        super().__init__(parent)
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.title_context = title
        self.info_map = {}
        self.page_height = 0

    def set_map(self, tmp):
        self.info_map = tmp

    def init_ui(self):
        group_box = QGroupBox(self.title_context)
        group_box.setObjectName('noneBorder')
        group_box.setFixedWidth(720)

        form_layout = QFormLayout()
        form_layout.setSpacing(utils.ITEMVSPACE)
        form_layout.setHorizontalSpacing(utils.ITEMHSPACE)
        form_layout.setRowWrapPolicy(QFormLayout.DontWrapRows)
        form_layout.setFieldGrowthPolicy(QFormLayout.FieldsStayAtSizeHint)

        for k, v in self.info_map.items():
            label = QLabel()
            label.setObjectName('grayLabel')
            label.setText('{}'.format(v if v is not None else ''))
            label.setFixedHeight(utils.ITEMHEIGHT)
            form_layout.addRow(k, label)
            form_layout.labelForField(label).setObjectName('grayLabel')
            form_layout.labelForField(label).setFixedHeight(utils.ITEMHEIGHT)
            self.page_height += label.height()

        self.page_height += utils.ITEMVSPACE * 2
        self.page_height += 30
        self.page_height += utils.ITEMHSPACE
        self.info_map.clear()

        group_box.setLayout(form_layout)
        layout = QVBoxLayout()
        layout.addWidget(group_box)
        layout.setContentsMargins(utils.ITEMHSPACE, utils.ITEMHSPACE, 0, 0)
        self.setLayout(layout)


if __name__ == '__main__':
    import sys
    from PyQt5.QtWidgets import QApplication

    app = QApplication(sys.argv)
    page = TemplatePage('测试模板页面')
    page.setFixedHeight(180)
    page.set_map({'模板名称': '测试'})
    page.init_ui()
    page.show()
    sys.exit(app.exec_())
