from PyQt5.QtWidgets import QLabel, QPushButton, QHBoxLayout, QVBoxLayout

import utils
from draggable_shadow_widget import DraggableShadowWidget
from title_bar import TitleBar


class MsgBox(DraggableShadowWidget):
    def __init__(self, skin, parent=None):
        super().__init__(parent)
        self.resize(280, 160)
        self.title_bar = TitleBar(self)
        self.title_bar.setFixedHeight(30)
        self.title_bar.set_title_width(self.width())
        self.title_bar.set_title_background(skin)

        self.msg_label = QLabel()
        self.msg_label.setFixedSize(45, 45)
        self.msg_label.setScaledContents(True)

        self.tip_label = QLabel()
        self.tip_label.setWordWrap(True)

        center_layout = QHBoxLayout()
        center_layout.addWidget(self.msg_label)
        center_layout.addWidget(self.tip_label)
        center_layout.setSpacing(10)
        center_layout.setContentsMargins(15, 0, 15, 0)

        self.ok_button = QPushButton()
        self.ok_button.setFixedSize(60, 25)
        self.ok_button.setText('确定')
        self.ok_button.setObjectName('msgButton')

        self.cancel_button = QPushButton()
        self.cancel_button.setFixedSize(60, 25)
        self.cancel_button.setText('取消')
        self.cancel_button.setObjectName('msgButton')

        bottom_layout = QHBoxLayout()
        bottom_layout.addStretch()
        bottom_layout.addWidget(self.ok_button)
        bottom_layout.addWidget(self.cancel_button)
        bottom_layout.setSpacing(10)
        bottom_layout.setContentsMargins(0, 0, 10, 10)

        main_layout = QVBoxLayout()
        main_layout.addWidget(self.title_bar)
        main_layout.addStretch()
        main_layout.addLayout(center_layout)
        main_layout.addStretch()
        main_layout.addLayout(bottom_layout)
        main_layout.setSpacing(0)
        main_layout.setContentsMargins(utils.SHADOW_WIDTH, utils.SHADOW_WIDTH, utils.SHADOW_WIDTH, utils.SHADOW_WIDTH)
        self.setLayout(main_layout)

        self.ok_button.clicked.connect(lambda: self.accept())
        self.cancel_button.clicked.connect(lambda: self.reject())
        self.title_bar.closeClicked.connect(lambda: self.reject())

    def set_info(self, title, info, pixmap, is_ok_hidden):
        self.title_bar.set_title_name(title)
        self.tip_label.setText(info)
        self.msg_label.setPixmap(pixmap)
        self.ok_button.setHidden(is_ok_hidden)
        if is_ok_hidden:
            self.cancel_button.setText('确定')
        else:
            self.ok_button.setText('确定')
            self.cancel_button.setText('取消')


if __name__ == '__main__':
    import sys
    from PyQt5.QtWidgets import QApplication
    from PyQt5.QtGui import QPixmap
    import res_rc

    app = QApplication(sys.argv)
    msg = MsgBox(":/background/skin/1.png")
    msg.set_info('标题', '您确定删除此账号的所有信息？', QPixmap(':/loginDialog/login/attention.png'), False)
    sys.exit(msg.exec_())
