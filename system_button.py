from enum import Enum

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtGui import QPixmap, QPainter


class ButtonStatus(Enum):
    NORMAL = 0
    ENTER = 1
    PRESS = 2


class SystemButton(QPushButton):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.status = ButtonStatus.NORMAL
        self.mouse_press = False

    def load_pixmap(self, pic_name):
        self.pixmap = QPixmap(pic_name)
        self.btn_width = self.pixmap.width() / 3
        self.btn_height = self.pixmap.height()
        self.setFixedSize(self.btn_width, self.btn_height)

    def enterEvent(self, event):
        self.status = ButtonStatus.ENTER
        self.update()

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.mouse_press = True
            self.status = ButtonStatus.PRESS
            self.update()

    def mouseReleaseEvent(self, event):
        if self.mouse_press and self.rect().contains(event.pos()):
            self.mouse_press = False
            self.status = ButtonStatus.ENTER
            self.update()
            self.clicked.emit()

    def leaveEvent(self, event):
        self.status = ButtonStatus.NORMAL
        self.update()

    def paintEvent(self, event):
        painter = QPainter()
        painter.begin(self)
        painter.drawPixmap(self.rect(),
                           self.pixmap.copy(self.btn_width * self.status.value, 0, self.btn_width, self.btn_height))
