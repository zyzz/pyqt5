import os
import cgi
import re
from encodings.aliases import aliases

from sqlalchemy import distinct
from PyQt5.QtCore import QSize, Qt, QFileInfo, pyqtSignal
from PyQt5.QtGui import QIntValidator
from PyQt5.QtWidgets import QWizard, QWizardPage, QFileDialog, QMessageBox, QTableWidgetItem, QComboBox, QCheckBox, \
    QButtonGroup, QDialog

from models import Session, ServerProfile, ImportProfile, Table, Field
from resources.ui import (
    ui_app_wizard_page_server,
    ui_app_wizard_page_type,
    ui_app_wizard_page_txt_data_source,
    ui_app_wizard_page_separator,
    ui_app_wizard_page_txt_additional_options,
    ui_app_wizard_page_txt_data_target,
    ui_app_wizard_page_target_schema,
    ui_app_wizard_page_mode,
    ui_app_wizard_page_file_strategy,
    ui_app_wizard_page_process
)
from template_name_setup import TemplateNameSetup


class AppWizard(QWizard):
    addApplication = pyqtSignal(str, str)

    def __init__(self):
        super().__init__()
        self.profile = ImportProfile()
        self.setOption(QWizard.HaveHelpButton, True)
        self.setButtonText(QWizard.HelpButton, '保存')
        self.save_button = self.button(QWizard.HelpButton)
        self.save_button.clicked.connect(self.save)
        self.currentIdChanged.connect(self.current_id_changed)

        self.resize(QSize(656, 519))
        self.addPage(WizardPageStepZero())
        self.addPage(WizardPageStepOne())
        self.addPage(WizardPageStepTwo())
        self.addPage(WizardPageStepThree())
        self.addPage(WizardPageStepFour())
        self.addPage(WizardPageStepFive())
        self.addPage(WizardPageStepSix())
        self.addPage(WizardPageStepSeven())
        self.addPage(WizardPageStepNine())
        self.addPage(WizardPageStepEight())

    def current_id_changed(self, page_id):
        self.save_button.setEnabled(page_id == self.pageIds()[-1])

    def save(self):
        dialog = TemplateNameSetup()
        if QDialog.Accepted == dialog.exec_():
            template_name = dialog.get_template_name()
        else:
            return

        self.profile.name = template_name
        session = Session()
        session.add(self.profile)
        try:
            session.commit()
        except Exception as e:
            QMessageBox.critical(None, '错误', str(e))
            session.close()
            return

        QMessageBox.information(None, '提示', '模板配置保存成功！')
        self.close()
        self.addApplication.emit(self.profile.name, self.profile.import_type)


class WizardPageStepZero(QWizardPage):
    def __init__(self):
        super().__init__()
        self.ui = ui_app_wizard_page_server.Ui_Form()
        self.ui.setupUi(self)

        self.ui.server_addr_combo_box.currentTextChanged.connect(self.on_server_addr_changed)

        session = Session()
        for result in session.query(distinct(ServerProfile.api_root)).all():
            self.ui.server_addr_combo_box.addItem(result[0])
        session.close()

    def validatePage(self):
        server_addr = self.ui.server_addr_combo_box.currentText()
        if not server_addr:
            QMessageBox.warning(None, '提示', '服务器地址不能为空！')
            self.ui.server_addr_combo_box.setFocus()
            return False
        app_id = self.ui.app_id_combo_box.currentText()
        if not app_id:
            QMessageBox.warning(None, '提示', '实例名称不能为空！')
            self.ui.app_id_combo_box.setFocus()
            return False

        api_root = self.ui.server_addr_combo_box.currentText()
        app_id = self.ui.app_id_combo_box.currentText()
        session = Session()
        result = session.query(ServerProfile.id).filter_by(api_root=api_root, application_id=app_id).first()
        if result is None:
            QMessageBox.critical(None, '错误', '获取数据库地址错误！')
            return False
        session.close()
        self.wizard().profile.server_id = result[0]
        return True

    def on_server_addr_changed(self, server_addr):
        self.ui.app_id_combo_box.clear()
        session = Session()
        for result in session.query(ServerProfile.application_id).filter_by(api_root=server_addr).all():
            self.ui.app_id_combo_box.addItem(result[0])
        session.close()


class WizardPageStepOne(QWizardPage):
    def __init__(self):
        super().__init__()
        self.ui = ui_app_wizard_page_type.Ui_Form()
        self.ui.setupUi(self)

        self.button_group = QButtonGroup()
        self.button_group.addButton(self.ui.radio_button_txt, 0)
        self.button_group.addButton(self.ui.radio_button_csv, 1)
        self.button_group.addButton(self.ui.radio_button_html, 2)
        self.button_group.addButton(self.ui.radio_button_xls, 3)
        self.button_group.addButton(self.ui.radio_button_xlsx, 4)
        self.button_group.addButton(self.ui.radio_button_json, 5)

    def cleanupPage(self):
        pass

    def validatePage(self):
        object_name = self.button_group.checkedButton().objectName()
        self.wizard().profile.import_type = object_name.split('_')[-1]
        return True


class WizardPageStepTwo(QWizardPage):
    def __init__(self, ):
        super().__init__()
        self.ui = ui_app_wizard_page_txt_data_source.Ui_Form()
        self.ui.setupUi(self)
        # self.ui.line_edit_source = LineEdit(self)
        self.ui.push_button_open_file.pressed.connect(self.open_file)
        self.ui.line_edit_source.doubleClick.connect(self.open_file)
        self.ui.combo_box_encoding.addItems(['{0} ({1})'.format(k, v) for k, v in aliases.items()])
        self.ui.combo_box_encoding.setCurrentText('chinese (gb2312)')

    def open_file(self):
        path, _ = QFileDialog.getOpenFileName(None, u'打开', '.', '文本文件(*.txt;*.csv;*.dat);;全部文件(*.*)')
        if path:
            self.ui.line_edit_source.setText(path)

    def validatePage(self):
        path = self.ui.line_edit_source.text()
        fi = QFileInfo(path)
        if not fi.isFile():
            QMessageBox.warning(None, '提示', '必须输入导入文件')
            return False

        encoding = self.ui.combo_box_encoding.currentText()
        match = re.match(".*\((.*)\).*", encoding)

        self.wizard().profile.source_file = os.path.abspath(path)
        self.wizard().profile.source_code_page = match.group(1)
        return True


class WizardPageStepThree(QWizardPage):
    def __init__(self):
        super().__init__()
        self.ui = ui_app_wizard_page_separator.Ui_Form()
        self.ui.setupUi(self)
        self.ui.combo_box_field_separator.currentIndexChanged.connect(self.current_index_changed)

        self.ui.combo_box_record_separator.addItem('CRLF', '0D0A')
        self.ui.combo_box_record_separator.addItem('CR', '0D')
        self.ui.combo_box_record_separator.addItem('LF', '0A')

        self.ui.combo_box_field_separator.addItem('定位', hex(ord('\t')))
        self.ui.combo_box_field_separator.addItem('分号(;)', hex(ord(';')))
        self.ui.combo_box_field_separator.addItem('逗号(,)', hex(ord(',')))
        self.ui.combo_box_field_separator.addItem('空格', hex(ord(' ')))
        self.ui.combo_box_field_separator.addItem('无', '')
        self.ui.combo_box_field_separator.addItem('其他符号')

        self.ui.combo_box_text_qualifier.addItem('无')
        self.ui.combo_box_text_qualifier.addItem('"', cgi.html.escape('"'))
        self.ui.combo_box_text_qualifier.addItem("'", cgi.html.escape("'"))
        self.ui.combo_box_text_qualifier.addItem('~', cgi.html.escape('~'))
        self.ui.combo_box_text_qualifier.setCurrentIndex(1)

        self.current_index_changed(self.ui.combo_box_field_separator.currentIndex())

    def current_index_changed(self, index):
        line_edit = self.ui.line_edit_other_separator
        line_edit.show() if index == self.ui.combo_box_field_separator.count() - 1 else line_edit.hide()

    def initializePage(self):
        pass

    def validatePage(self):
        row_delimiter = self.ui.combo_box_record_separator.itemData(
            self.ui.combo_box_record_separator.currentIndex())
        self.wizard().profile.row_delimiter = row_delimiter

        if self.ui.combo_box_field_separator.currentText() == '其他符号':
            other_separator = self.ui.line_edit_other_separator.text()
            if other_separator:
                field_delimiter = hex(ord(other_separator))
        elif self.ui.combo_box_field_separator.currentText() != '无':
            field_delimiter = self.ui.combo_box_field_separator.itemData(
                self.ui.combo_box_field_separator.currentIndex())
        self.wizard().profile.field_delimiter = field_delimiter

        if self.ui.combo_box_text_qualifier.currentText() != '无':
            self.wizard().profile.text_qualifier = self.ui.combo_box_text_qualifier.itemData(
                self.ui.combo_box_text_qualifier.currentIndex())
        return True


class WizardPageStepFour(QWizardPage):
    def __init__(self):
        super().__init__()
        self.ui = ui_app_wizard_page_txt_additional_options.Ui_Form()
        self.ui.setupUi(self)
        validator = QIntValidator()
        self.ui.line_edit_field_line.setValidator(validator)
        self.ui.line_edit_first_data_line.setValidator(validator)
        self.ui.line_edit_last_data_line.setValidator(validator)

    def validatePage(self):
        self.wizard().profile.field_name_row = int(self.ui.line_edit_field_line.text())
        self.wizard().profile.first_row = int(self.ui.line_edit_first_data_line.text())
        if self.ui.line_edit_last_data_line.text():
            self.wizard().profile.last_row = self.ui.line_edit_last_data_line.text()
        return True


class WizardPageStepFive(QWizardPage):
    def __init__(self):
        super().__init__()
        self.ui = ui_app_wizard_page_txt_data_target.Ui_Form()
        self.ui.setupUi(self)

    def initializePage(self):
        table_widget = self.ui.table_widget
        file_info = QFileInfo(self.wizard().profile.source_file)
        table_widget.setRowCount(1)
        item = QTableWidgetItem(file_info.baseName())
        item.setData(Qt.UserRole, file_info.absoluteFilePath())
        table_widget.setItem(0, 0, item)
        combo_box = QComboBox()
        combo_box.addItem(file_info.baseName())
        table_widget.setCellWidget(0, 1, combo_box)
        check_box = QCheckBox()
        check_box.setChecked(True)
        # check_box.setEnabled(False)
        table_widget.setCellWidget(0, 2, check_box)

    def validatePage(self):
        self.wizard().profile.tables.clear()
        for i in range(self.ui.table_widget.rowCount()):
            table = Table()
            table.source_table_name = self.ui.table_widget.item(i, 0).text()
            table.source_file = self.ui.table_widget.item(i, 0).data(Qt.UserRole)
            table.target_table_name = self.ui.table_widget.cellWidget(i, 1).currentText()
            if not table.target_table_name:
                QMessageBox.information(None, '提示', '目标对象不能为空!')
                self.cleanupPage()
                return
            table.is_new_table = Qt.Checked == self.ui.table_widget.cellWidget(i, 2).checkState()
            self.wizard().profile.tables.append(table)
        return True


class WizardPageStepSix(QWizardPage):
    def __init__(self):
        super().__init__()
        self.ui = ui_app_wizard_page_target_schema.Ui_Form()
        self.ui.setupUi(self)
        self.ui.combo_box_source.currentIndexChanged.connect(self.current_index_changed)

    def initializePage(self):
        self.ui.combo_box_source.clear()
        profile = self.wizard().profile
        for table in profile.tables:
            table.fields.clear()
            with open(table.source_file, encoding=profile.source_code_page) as f:
                field_row = f.readlines()[profile.field_name_row - 1: profile.field_name_row][0]
                delimiter = chr(int(profile.field_delimiter, 16))
                for index, name in enumerate(field_row.split(delimiter)):
                    field = Field()
                    field.source_field_name = 'f{0}'.format(index + 1)
                    field.target_field_name = name
                    field.source_field_create_name = table.target_table_name
                    table.fields.append(field)
            self.ui.combo_box_source.addItem(table.source_table_name, table.target_table_name)
        self.ui.combo_box_source.setCurrentIndex(0)

    def get_profile_table_by_target_table_name(self, target_table_name):
        return next(t for t in self.wizard().profile.tables if t.target_table_name == target_table_name)

    def get_profile_table_by_source_table_name(self, source_table_name):
        return next(t for t in self.wizard().profile.tables if t.source_table_name == source_table_name)

    def current_index_changed(self, index):
        table_widget = self.ui.tableWidget
        if index == -1:
            table_widget.clearContents()
            table_widget.setRowCount(0)
            self.ui.label_target.setText('')
            return
        source_table_name = self.ui.combo_box_source.itemData(index)
        target_table_name = self.ui.label_target.text()

        if target_table_name:
            table = self.get_profile_table_by_target_table_name(target_table_name)
            for i in range(table_widget.rowCount()):
                target_field_name = table_widget.item(i, 0).text()
                source_field_create_name = table_widget.item(i, 0).data(Qt.UserRole)
                field = next(f for f in table.fields if f.source_field_create_name == source_field_create_name)
                field.target_field_name = target_field_name

        table_widget.clearContents()
        table_widget.setRowCount(0)
        table = self.get_profile_table_by_source_table_name(source_table_name)
        self.ui.label_target.setText(table.target_table_name)
        table_widget.setRowCount(len(table.fields))
        for index, field in enumerate(table.fields):
            item = QTableWidgetItem(field.target_field_name)
            item.setData(Qt.UserRole, field.source_field_create_name)
            table_widget.setItem(index, 0, item)
            combo_box = QComboBox()
            combo_box.addItems(['TEXT', 'INTEGER', 'REAL', 'BLOB'])
            table_widget.setCellWidget(index, 1, combo_box)


class WizardPageStepSeven(QWizardPage):
    def __init__(self):
        super().__init__()
        self.ui = ui_app_wizard_page_mode.Ui_Form()
        self.ui.setupUi(self)

        self.button_group = QButtonGroup(self)
        self.button_group.addButton(self.ui.radio_button_add, 0)
        self.button_group.addButton(self.ui.radio_button_update, 1)
        self.button_group.addButton(self.ui.radio_button_add_or_update, 2)
        self.button_group.addButton(self.ui.radio_button_delete, 3)
        self.button_group.addButton(self.ui.radio_button_copy, 4)

    def validatePage(self):
        object_name = self.button_group.checkedButton().objectName()
        self.wizard().profile.import_mode = object_name[len('radio_button_'):]
        return True


class WizardPageStepEight(QWizardPage):
    def __init__(self):
        super().__init__()
        self.ui = ui_app_wizard_page_process.Ui_Form()
        self.ui.setupUi(self)

    def validatePage(self):
        pass


class WizardPageStepNine(QWizardPage):
    def __init__(self):
        super().__init__()
        self.ui = ui_app_wizard_page_file_strategy.Ui_Form()
        self.ui.setupUi(self)
        self.ui.folder_line_edit.doubleClick.connect(self.on_folder_button_clicked)
        self.ui.folder_push_button.clicked.connect(self.on_folder_button_clicked)
        self.ui.file_line_edit.doubleClick.connect(self.on_file_button_clicked)
        self.ui.file_push_button.clicked.connect(self.on_file_button_clicked)

    def on_folder_button_clicked(self):
        file_dir = QFileDialog.getExistingDirectory(None, u'选择文件夹', '.',
                                                    QFileDialog.ShowDirsOnly | QFileDialog.DontResolveSymlinks)
        if file_dir:
            self.ui.folder_line_edit.setText(file_dir)

    def on_file_button_clicked(self):
        path, _ = QFileDialog.getOpenFileName(None, u'打开', '.', '文本文件(*.txt;*.csv;*.dat);;全部文件(*.*)')
        if path:
            file_info = QFileInfo(path)
            self.ui.file_line_edit.setText(file_info.fileName())

    def initializePage(self):
        self.wizard().profile.watch_folder = None
        self.wizard().profile.watch_file = None
        self.wizard().profile.watch_file_regex = None
        self.wizard().profile.polling_interval = None

    def validatePage(self):
        folder = self.ui.folder_line_edit.text()
        if not folder:
            QMessageBox.warning(None, '提示', '文件夹不能为空！')
            self.ui.folder_line_edit.setFocus()
            return False
        file = self.ui.file_line_edit.text()
        if not file:
            QMessageBox.warning(None, '提示', '文件不能为空！')
            self.ui.file_line_edit.setFocus()
            return False
        regex = self.ui.regex_radio_button.isChecked()
        self.wizard().profile.watch_folder = os.path.abspath(folder)
        self.wizard().profile.watch_file = file
        self.wizard().profile.watch_file_regex = regex
        return True


if __name__ == '__main__':
    import sys
    from PyQt5.QtWidgets import QApplication

    app = QApplication(sys.argv)
    wizard = AppWizard()
    wizard.setWindowTitle("应用向导")
    wizard.show()
    sys.exit(app.exec_())

    import pandas as pd

    options = {
        'encoding': 'gb2312',
        'delimiter': chr(0x2c),
        'skiprows': 1,
        'names': ['1', '2', '3', '4', '5']
    }
    data_frame = pd.read_table('F:/DataCollectionCenter/0001AS_Dev.DAT', **options)
    print(data_frame.columns.values)
