from PyQt5.QtCore import Qt, pyqtSignal, QRect, QPoint
from PyQt5.QtWidgets import QWidget, QLabel, QHBoxLayout, QVBoxLayout
from PyQt5.QtGui import QPalette, QBrush, QPixmap

from system_button import SystemButton


class TitleBar(QWidget):
    closeClicked = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent, flags=Qt.FramelessWindowHint)
        self.setAutoFillBackground(True)

        self.close_btn = SystemButton(self)
        self.close_btn.setFocusPolicy(Qt.NoFocus)
        self.close_btn.load_pixmap(':/sys/sysBtn/close_button.png')

        self.title_label = QLabel(self)
        self.title_label.setObjectName('titleBarLabel')
        self.title_label.setAlignment(Qt.AlignCenter)
        self.close_btn.clicked.connect(self.closeClicked)

        title_layout = QHBoxLayout()
        title_layout.addStretch()
        title_layout.addWidget(self.title_label, 0, Qt.AlignVCenter)
        title_layout.addStretch()
        title_layout.addWidget(self.close_btn, 0, Qt.AlignTop)
        title_layout.setContentsMargins(0, 0, 0, 0)

        main_layout = QVBoxLayout()
        main_layout.addLayout(title_layout)
        main_layout.addStretch()
        main_layout.setContentsMargins(self.close_btn.width(), 0, 0, 0)
        self.setLayout(main_layout)

        self.title_width = 300
        self.title_name = ''
        self.background_image = ''
        self.left_btn_pressed = False
        self.press_point = QPoint()

    def reset_background(self, name):
        self.background_image = name
        palette = QPalette()
        palette.setBrush(QPalette.Background, QBrush(QPixmap(self.background_image)))
        self.setPalette(palette)

    def set_title_width(self, width):
        self.title_width = width

    def set_title_name(self, name):
        self.title_name = name

    def set_title_background(self, name):
        self.background_image = name

    def resizeEvent(self, event):
        self.setFixedWidth(self.title_width)

        palette = QPalette()
        palette.setBrush(QPalette.Background, QBrush(QPixmap(self.background_image)))
        self.setPalette(palette)

        self.title_label.setText(self.title_name)
        self.title_label.setGeometry(QRect(40, 0, self.title_width - 80, 32))

    def mousePressEvent(self, event):
        if event.button == Qt.LeftButton:
            if event.y() < 5 or event.x() < 5 or self.rect().width() - event.x() < 5:
                event.ignore()
                return
            self.press_point = event.globalPos()
            self.left_btn_pressed = True
        event.ignore()

    def mouseMoveEvent(self, event):
        if self.left_btn_pressed:
            move_point = event.globalPos()
            main_window = self.parent()
            main_window.move(main_window.pos() + move_point - self.press_point)
            self.press_point = move_point
        event.ignore()

    def mouseReleaseEvent(self, event):
        if event.button == Qt.LeftButton:
            self.left_btn_pressed = False
        event.ignore()


if __name__ == '__main__':
    import sys

    from PyQt5.QtWidgets import QApplication

    import res_rc

    app = QApplication(sys.argv)
    title_bar = TitleBar()
    title_bar.set_title_name('标题')
    title_bar.show()
    sys.exit(app.exec_())
