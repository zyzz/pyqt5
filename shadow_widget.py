from PyQt5.QtCore import pyqtProperty, Qt
from PyQt5.QtWidgets import QWidget
from PyQt5.QtGui import QColor, QPainter


class ShadowWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent, flags=Qt.FramelessWindowHint | Qt.Dialog)

        self.widget_opacity = 1
        self.widget_color = QColor()

    @pyqtProperty(float)
    def opacity(self):
        return self.widget_opacity

    @opacity.setter
    def opacity(self, opacity):
        self.widget_opacity = opacity
        self.update()

    def set_color(self, color):
        self.widget_color = color

    def paintEvent(self, painter_event):
        painter = QPainter(self)
        painter.setOpacity(self.widget_opacity)
        painter.setBrush(self.widget_color)
        painter.setPen(Qt.NoPen)
        painter.drawRect(self.rect())


if __name__ == '__main__':
    import sys
    from PyQt5.QtWidgets import QApplication, QDialog

    app = QApplication(sys.argv)

    shadow_widget = ShadowWidget()
    shadow_widget.set_color(QColor("#e9eef0"))
    shadow_widget.show()
    sys.exit(app.exec_())
