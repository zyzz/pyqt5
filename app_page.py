import cgi
from collections import OrderedDict

from PyQt5.QtCore import QObject, QSize
from PyQt5.QtWidgets import QListWidgetItem
from PyQt5.QtGui import QIcon, QPixmap

from models import Session, ImportProfile
from app_wizard import AppWizard
from scroll_widget import ScrollWidget
from template_page import TemplatePage


class AppPage(QObject):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.main_window = parent
        self.wizard = None

    def init_ui(self):
        self.main_window.app_action.setObjectName('transparentWidget')
        self.main_window.app_wizard_button.setObjectName('greenButton')
        self.main_window.app_action_tip_label.setObjectName('whiteLabel')

        self.main_window.app_list_widget.setObjectName('infoList')
        self.main_window.app_list_widget.currentRowChanged.connect(self.on_change_info_page)

        session = Session()
        for profile in session.query(ImportProfile).all():
            self.on_add_application(profile.name, profile.import_type)
        session.close()

        if self.main_window.app_list_widget.count() > 0:
            self.main_window.app_list_widget.setCurrentRow(0)

    def init_connect(self):
        self.main_window.app_wizard_button.clicked.connect(self.on_app_wizard_button_clicked)

    def on_app_wizard_button_clicked(self):
        self.wizard = AppWizard()
        self.wizard.addApplication.connect(self.on_add_application)
        self.wizard.show()

    def on_add_application(self, name, type_name):
        item = QListWidgetItem(name)
        item.setSizeHint(QSize(120, 36))
        item.setIcon(QIcon(QPixmap(':/page/app/{0}.png'.format(type_name))))
        self.main_window.app_list_widget.addItem(item)
        self.main_window.app_list_widget.setCurrentRow(self.main_window.app_list_widget.count() - 1)

    def on_change_info_page(self, current_row):
        app_name = self.main_window.app_list_widget.item(current_row).text()

        widget = self.main_window.app_stacked_widget.currentWidget()
        if widget is not None:
            self.main_window.app_stacked_widget.removeWidget(widget)

        session = Session()

        profile = session.query(ImportProfile).filter_by(name=app_name).one()
        print([field.to_dict() for field in profile.tables[0].fields])

        scroll_widget = ScrollWidget()
        template_page = TemplatePage('导入格式', scroll_widget.zone)
        template_page.set_map({"导入类型": profile.import_type})
        template_page.init_ui()
        template_page.show()
        scroll_widget.add_scroll_widget(template_page)

        template_page = TemplatePage('数据源', scroll_widget.zone)
        items = OrderedDict()
        items['文件位置'] = profile.source_file
        items['文件编码'] = profile.source_code_page
        template_page.set_map(items)
        template_page.init_ui()
        template_page.show()
        scroll_widget.add_scroll_widget(template_page)

        template_page = TemplatePage('分隔符', scroll_widget.zone)
        items = OrderedDict()
        items['记录分隔符'] = profile.row_delimiter.replace('0D', 'CR').replace('0A', 'LF')
        items['字段分隔符'] = self._get_field_delimiter(profile.field_delimiter)
        items['文本限定符'] = cgi.html.unescape(profile.text_qualifier)
        template_page.set_map(items)
        template_page.init_ui()
        template_page.show()

        scroll_widget.add_scroll_widget(template_page)

        template_page = TemplatePage('附加选项', scroll_widget.zone)
        items = OrderedDict()
        items['字段名行'] = profile.field_name_row
        items['第一个数据行'] = profile.first_row
        items['最后一个数据行'] = profile.last_row
        template_page.set_map(items)
        template_page.init_ui()
        template_page.show()
        scroll_widget.add_scroll_widget(template_page)

        template_page = TemplatePage('导入模式', scroll_widget.zone)
        items = OrderedDict()
        items['模式'] = profile.import_mode
        template_page.set_map(items)
        template_page.init_ui()
        template_page.show()
        scroll_widget.add_scroll_widget(template_page)

        session.close()

        self.main_window.app_stacked_widget.addWidget(scroll_widget)
        self.main_window.app_stacked_widget.setCurrentWidget(scroll_widget)

    @staticmethod
    def _get_field_delimiter(delimiter):
        if delimiter == hex(ord('\t')):
            return '定位'
        elif delimiter == hex(ord(';')):
            return '分号(;）'
        elif delimiter == hex(ord(',')):
            return '逗号(,)'
        elif delimiter == hex(ord(' ')):
            return '空格'
        elif delimiter == '':
            return '无'
        else:
            return delimiter
