from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QScrollArea


class CardWidget(QWidget):
    def __init__(self, card_width=0, card_height=0, space=0, parent=None):
        super().__init__(parent)
        self.card_count = 0
        self.number_per_row = -1
        self.item_width = card_width
        self.item_height = card_height
        self.card_space = space

        self.scroll_area = QScrollArea(self)
        self.scroll_area.setStyleSheet("QScrollArea{border: none;background-color: #ffffff;}")
        self.card_panel = QWidget()
        self.card_panel.setStyleSheet("QWidget{border: none;background-color: #ffffff;}")
        self.scroll_area.setWidget(self.card_panel)
        self.scroll_area.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)

    def calculate_data(self):
        self.scroll_area.setGeometry(0, 0, self.width(), self.height())
        self.card_panel.setGeometry(0, 0, self.width(), self.height())
        self.number_per_row = int((self.width() + self.card_space) / (self.item_width + self.card_space))

    def add_card(self, card):
        x = int(self.card_count % self.number_per_row) * (self.item_width + self.card_space)
        y = int(self.card_count / self.number_per_row) * (self.item_height + self.card_space)

        now_height = y + self.item_height
        if now_height >= self.card_panel.height():
            self.card_panel.resize(self.card_panel.width(), now_height)
        card.move(x, y)
        self.card_count += 1
        card.setVisible(True)

    def clear_card(self):
        for child in self.card_panel.children():
            child.deleteLater()
        self.card_panel.setGeometry(0, 0, self.width(), self.height())
        self.card_count = 0
