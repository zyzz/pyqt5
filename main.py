import sys

from PyQt5.QtWidgets import qApp, QMainWindow
from PyQt5.QtCore import QTranslator, QFile

import res_rc
from single_application import QSingleApplication
from splash_screen import SplashScreen
from main_window import MainWindow

if __name__ == '__main__':
    application_name = 'DAQ'
    # QApplication.addLibraryPath("./plugins")
    app = QSingleApplication(application_name, sys.argv)

    trans = QTranslator()
    trans.load('qt_zh_CN')
    app.installTranslator(trans)

    qss = QFile(':/qssfile/qss/mmap.qss')
    qss.open(QFile.ReadOnly)
    style = bytes(qss.readAll()).decode('utf-8')
    qApp.setStyleSheet(style)
    qss.close()

    app.setApplicationVersion("1.0")
    app.setOrganizationName("MmapSoft")
    app.setApplicationName(application_name)

    if app.is_running():
        sys.exit(0)

    # splash = SplashScreen(1, windowsoptions['splashimg'])
    main_window = MainWindow()
    main_window.show()
    # splash.finish(mainwindow)

    # app.set_activation_window(main_window)

    # main_window.guimanger.globals = globals()
    # main_window.guimanger.locals = locals()

    # print(app.desktop().availableGeometry())
    # main_window.setGeometry(app.desktop().screen().geometry())

    exitCode = app.exec_()
    sys.exit(exitCode)
