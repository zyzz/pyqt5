from PyQt5.QtWidgets import QDialog
from PyQt5.QtCore import Qt, QRect, QPoint
from PyQt5.QtGui import QPainter, QPixmap
from utils import SHADOW_WIDTH


class DraggableShadowWidget(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent, flags=Qt.FramelessWindowHint | Qt.Dialog)
        self.setAttribute(Qt.WA_TranslucentBackground)
        self.move_point = QPoint()
        self.mouse_press = False
        self.shadow = True

        self.pixmaps = [
            QPixmap(":/shadow/shadow_left"),
            QPixmap(":/shadow/shadow_right"),
            QPixmap(":/shadow/shadow_top"),
            QPixmap(":/shadow/shadow_bottom"),
            QPixmap(":/shadow/shadow_left_top"),
            QPixmap(":/shadow/shadow_right_top"),
            QPixmap(":/shadow/shadow_left_bottom"),
            QPixmap(":/shadow/shadow_right_bottom")
        ]

    def enable_shadow(self, enable):
        self.shadow = enable
        self.update()

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.mouse_press = True

        self.move_point = event.globalPos() - self.pos()

    def mouseReleaseEvent(self, event):
        self.mouse_press = False
        self.setWindowOpacity(1)

    def mouseMoveEvent(self, event):
        if self.mouse_press:
            self.move(event.globalPos() - self.move_point)
            self.setWindowOpacity(0.9)

    def paintEvent(self, *args, **kwargs):
        if self.shadow:
            painter = QPainter(self)
            self.draw_window_shadow(painter)
            painter.setPen(Qt.NoPen)
            painter.setBrush(Qt.white)
            painter.drawRect(
                QRect(SHADOW_WIDTH, SHADOW_WIDTH, self.width() - 2 * SHADOW_WIDTH, self.height() - 2 * SHADOW_WIDTH))

    def draw_window_shadow(self, painter):

        painter.drawPixmap(0, 0, SHADOW_WIDTH, SHADOW_WIDTH, self.pixmaps[4])
        painter.drawPixmap(self.width() - SHADOW_WIDTH, 0, SHADOW_WIDTH, SHADOW_WIDTH, self.pixmaps[5])
        painter.drawPixmap(0, self.height() - SHADOW_WIDTH, SHADOW_WIDTH, SHADOW_WIDTH, self.pixmaps[6])
        painter.drawPixmap(self.width() - SHADOW_WIDTH, self.height() - SHADOW_WIDTH, SHADOW_WIDTH, SHADOW_WIDTH,
                           self.pixmaps[7])
        painter.drawPixmap(0, SHADOW_WIDTH, SHADOW_WIDTH, self.height() - 2 * SHADOW_WIDTH,
                           self.pixmaps[0].scaled(SHADOW_WIDTH, self.height() - 2 * SHADOW_WIDTH))
        painter.drawPixmap(self.width() - SHADOW_WIDTH, SHADOW_WIDTH, SHADOW_WIDTH, self.height() - 2 * SHADOW_WIDTH,
                           self.pixmaps[1].scaled(SHADOW_WIDTH, self.height() - 2 * SHADOW_WIDTH))
        painter.drawPixmap(SHADOW_WIDTH, 0, self.width() - 2 * SHADOW_WIDTH, SHADOW_WIDTH,
                           self.pixmaps[2].scaled(self.width() - 2 * SHADOW_WIDTH, SHADOW_WIDTH))
        painter.drawPixmap(SHADOW_WIDTH, self.height() - SHADOW_WIDTH, self.width() - 2 * SHADOW_WIDTH, SHADOW_WIDTH,
                           self.pixmaps[3].scaled(self.width() - 2 * SHADOW_WIDTH, SHADOW_WIDTH))
