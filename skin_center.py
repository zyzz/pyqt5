import os

from PyQt5.QtCore import Qt, QRect, QPropertyAnimation, QParallelAnimationGroup, QStandardPaths, QDir, QSignalMapper, \
    QFile, QFileInfo
from PyQt5.QtWidgets import QDialog, QFileDialog
from PyQt5.QtGui import QPalette, QBrush, QPixmap

from draggable_shadow_widget import DraggableShadowWidget
from resources.ui import ui_skin_center
from card_widget import CardWidget
from item_card import ItemCard


class SkinCenter(DraggableShadowWidget):
    def __init__(self, skin, parent=None):
        super().__init__(parent)
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.ui = ui_skin_center.Ui_Form()
        self.ui.setupUi(self)
        self.last_skin_path = skin

        palette = self.palette()
        palette.setBrush(QPalette.Background, QBrush(QPixmap(skin)))
        self.ui.base_widget.setPalette(palette)

        self.ui.close_btn.setFocusPolicy(Qt.NoFocus)
        self.ui.close_btn.load_pixmap(':/sys/sysBtn/close_button.png')

        self.main_window = None

        self.about_group = None
        self.contributor_group = None

        self.list_widget = None
        self.custom_list_widget = None

        self.card_list = []
        self.custom_card_list = []

        self.init_animation()
        self.init_connect()

    def set_parent_window(self, window):
        self.main_window = window

    def init_animation(self):
        main_action_rect = QRect(10, 75, 60, 2)
        orig_action_rect = QRect(75, 75, 60, 2)

        about_animation = QPropertyAnimation(self.ui.indicator, b'geometry')
        about_animation.setDuration(300)
        about_animation.setStartValue(orig_action_rect)
        about_animation.setEndValue(main_action_rect)

        self.about_group = QParallelAnimationGroup(self)
        self.about_group.addAnimation(about_animation)

        contributor_animation = QPropertyAnimation(self.ui.indicator, b'geometry')
        contributor_animation.setDuration(300)
        contributor_animation.setStartValue(main_action_rect)
        contributor_animation.setEndValue(orig_action_rect)

        self.contributor_group = QParallelAnimationGroup(self)
        self.contributor_group.addAnimation(contributor_animation)

    def init_connect(self):
        self.ui.sys_btn.clicked.connect(self.show_system)
        self.ui.custom_btn.clicked.connect(self.show_custom)

    def show_system(self):
        self.about_group.start()
        if self.list_widget is not None:
            self.list_widget.show()
        if self.custom_list_widget is not None:
            self.custom_list_widget.hide()

    def show_custom(self):
        self.contributor_group.start()
        if self.custom_list_widget is not None:
            self.custom_list_widget.show()
        if self.list_widget is not None:
            self.list_widget.hide()

    def change_skin_center_background(self, picture):
        self.last_skin_path = picture
        palette = QPalette()
        palette.setBrush(QPalette.Background, QBrush(QPixmap(self.last_skin_path)))
        self.ui.base_widget.setPalette(palette)

        self.main_window.change_skin(picture)

    def change_enter_background(self, picture):
        self.main_window.review_the_point_skin(picture)
        palette = QPalette()
        palette.setBrush(QPalette.Background, QBrush(QPixmap(picture)))
        self.ui.base_widget.setPalette(palette)

    def change_leave_background(self):
        self.main_window.review_the_org_skin()
        palette = QPalette()
        palette.setBrush(QPalette.Background, QBrush(QPixmap(self.last_skin_path)))
        self.ui.base_widget.setPalette(palette)

    def delete_background(self, picture):
        if QFile.remove(picture):
            self.custom_list_widget.clear_card()
            self.reload_background_list()
            conf_skin = self.main_window.get_current_background_abs_name()
            if conf_skin == picture:
                self.change_skin_center_background(":/background/skin/1.png")

    def init_sys_background_list(self):
        self.list_widget = CardWidget(130, 87, 10, self.ui.skin_widget)
        self.list_widget.setGeometry(QRect(0, 0, 420, 240))
        self.list_widget.calculate_data()

        app_data_location = QStandardPaths.writableLocation(QStandardPaths.AppConfigLocation)
        path = os.path.abspath(os.path.join(app_data_location, 'default'))
        pic_dir = QDir(path)
        pic_dir.setFilter(QDir.Files | QDir.Hidden | QDir.NoSymLinks)
        pic_dir.setSorting(QDir.Size | QDir.Reversed)
        filters = ['*.jpg', '*.png']
        pic_dir.setNameFilters(filters)
        entry_info_list = pic_dir.entryInfoList()
        cur_skin = self.main_window.get_current_background_name()
        self.card_list.clear()

        signal_mapper = QSignalMapper(self)
        for index, file_info in enumerate(entry_info_list):
            card = ItemCard(file_info.fileName(), False, self.list_widget.card_panel)
            self.card_list.append(card)
            if cur_skin == file_info.fileName():
                card.show_using_log(True)
            self.list_widget.add_card(card)
            card.sendBackgroundName.connect(signal_mapper.map)
            signal_mapper.setMapping(card, str(index))
            signal_mapper.mapped[str].connect(self.switch_using_logo)
            card.sendBackgroundName.connect(self.change_skin_center_background)
            card.sendEnterBackground.connect(self.change_enter_background)
            card.sendLeaveBackground.connect(self.change_leave_background)

        self.custom_list_widget = CardWidget(130, 87, 10, self.ui.skin_widget)
        self.custom_list_widget.hide()
        self.custom_card_list.clear()
        self.custom_list_widget.setGeometry(QRect(0, 0, 420, 240))
        self.custom_list_widget.calculate_data()
        self.reload_background_list()
        self.custom_list_widget.clear_card()
        self.reload_background_list()

    def reload_background_list(self):
        self.custom_list_widget.setGeometry(QRect(0, 2, 500, 230))
        self.custom_list_widget.calculate_data()

        app_data_location = QStandardPaths.writableLocation(QStandardPaths.AppConfigLocation)
        path = os.path.abspath(os.path.join(app_data_location, 'custom'))
        pic_dir = QDir(path)
        pic_dir.setFilter(QDir.Files | QDir.Hidden | QDir.NoSymLinks)
        pic_dir.setSorting(QDir.Size | QDir.Reversed)
        filters = ['*.jpg', '*.png']
        pic_dir.setNameFilters(filters)
        entry_info_list = pic_dir.entryInfoList()
        cur_skin = self.main_window.get_current_background_name()
        self.custom_card_list.clear()

        custom_mapper = QSignalMapper(self)
        for index, file_info in enumerate(entry_info_list):
            card = ItemCard(file_info.fileName(), True, self.custom_list_widget.card_panel)
            card.reset_display_del_btn_value(True)
            self.custom_card_list.append(card)
            if cur_skin == file_info.fileName():
                card.show_using_log(True)
            self.custom_list_widget.add_card(card)
            card.sendBackgroundName.connect(custom_mapper.map)
            custom_mapper.setMapping(card, str(index))
            custom_mapper.mapped[str].connect(self.switch_custom_using_logo)
            card.sendBackgroundName.connect(self.change_skin_center_background)
            card.sendEnterBackground.connect(self.change_enter_background)
            card.sendLeaveBackground.connect(self.change_leave_background)
            card.sendDeleteSignal.connect(self.delete_background)

        card = ItemCard(':/create.png', True, self.custom_list_widget.card_panel)
        self.custom_card_list.append(card)
        self.custom_list_widget.add_card(card)
        card.sendAddSignal.connect(self.add_custom_background)

    def add_custom_background(self):
        fd = QFileDialog(self)
        fd.resize(500, 471)

        app_data_location = QStandardPaths.writableLocation(QStandardPaths.AppConfigLocation)
        path = os.path.abspath(os.path.join(app_data_location, 'custom'))

        fd.setNameFilter('Image Files(*.jpg *.png)')
        fd.setViewMode(QFileDialog.List)
        if fd.exec_() == QDialog.Accepted:
            file_name_list = fd.selectedFiles()
            file_name = file_name_list[0]
            file_info = QFileInfo(file_name)
            new_file_name = os.path.join(path, file_info.fileName())
            if QFile.copy(file_name, new_file_name):
                self.change_skin_center_background(new_file_name)

                for card in self.custom_card_list:
                    visible = False
                    if card.get_card_name() == new_file_name:
                        visible = True
                    card.show_using_log(visible)

                for card in self.card_list:
                    card.show_using_log(False)
        else:
            fd.close()

    def switch_using_logo(self, index):
        current_index = int(index)
        for i, card in enumerate(self.card_list):
            card.show_using_log(i == current_index)

        for card in self.custom_card_list:
            card.show_using_log(False)

    def switch_custom_using_logo(self, index):
        current_index = int(index)
        for i, card in enumerate(self.custom_card_list):
            card.show_using_log(i == current_index)

        for card in self.card_list:
            card.show_using_log(False)

    def closeEvent(self, event):
        palette = QPalette()
        palette.setBrush(QPalette.Background, QBrush(QPixmap(self.last_skin_path)))
        self.ui.base_widget.setPalette(palette)
        self.main_window.restore_skin()


if __name__ == '__main__':
    import sys
    from PyQt5.QtWidgets import QApplication
    import res_rc

    app = QApplication(sys.argv)
    skin_center = SkinCenter(':/background/skin/1.png')
    skin_center.show()
    sys.exit(app.exec_())
