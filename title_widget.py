from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QVBoxLayout

import res_rc
from system_button import SystemButton


class TitleWidget(QWidget):
    closeApp = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)
        self.main_window = parent

    def init_ui(self):
        self.main_window.min_btn.load_pixmap(':/sys/sysBtn/min_button.png')
        self.main_window.close_btn.load_pixmap(':/sys/sysBtn/close_button.png')
        self.main_window.skin_btn.load_pixmap(':/sys/sysBtn/skin_button.png')
        self.main_window.main_menu_btn.load_pixmap(':/sys/sysBtn/main_menu.png')

    def init_connect(self):
        self.main_window.min_btn.clicked.connect(self.main_window.showMinimized)
        self.main_window.close_btn.clicked.connect(self.main_window.close)
        self.main_window.skin_btn.clicked.connect(self.main_window.open_skin_center)
        # self.main_menu_button.clicked.connect(self.main_window.show_main_menu)
