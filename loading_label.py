from PyQt5.QtCore import Qt, QTimer
from PyQt5.QtWidgets import QLabel
from PyQt5.QtGui import QPixmap, QBitmap


class LoadingLabel(QLabel):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.category = ''
        self.page_count = 1
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setAttribute(Qt.WA_TranslucentBackground, True)
        self.current_page = 0
        self.png_list = []
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.change_animation_step)

    def set_flag(self, flag):
        self.category = flag
        if self.category == "clean":
            self.page_count = 18
            self.setFixedSize(96, 96)
        elif self.category == 'working':
            self.page_count = 36
            self.setFixedSize(130, 130)

        self.load_all_pictures()
        self.raise_()
        cur_pix = self.png_list[0]
        self.setPixmap(cur_pix)
        self.setMask(QBitmap(cur_pix.mask()))

    def change_animation_step(self):
        if self.current_page == self.page_count:
            self.current_page = 0

        cur_pix = self.png_list[self.current_page]
        self.setPixmap(cur_pix)
        self.setMask(QBitmap(cur_pix.mask()))
        self.current_page += 1

    def load_all_pictures(self):
        if self.category == 'template':
            path = ':/movie/loading/'
        elif self.category == 'working':
            path = ':/movie/working/'

        for i in range(self.page_count):
            img = QPixmap(path + str(i + 1) + '.png')
            self.png_list.append(img)

    def start_loading(self):
        self.current_page = 0
        self.timer.start(50)

    def stop_loading(self):
        self.timer.stop()
