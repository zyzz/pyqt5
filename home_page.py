from PyQt5.QtCore import QObject

from login_dialog import LoginDialog


class HomePage(QObject):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.login_dialog = None

    def init_ui(self):
        main_window = self.parent()
        main_window.home_action.setObjectName('transparentWidget')
        main_window.home_action_tip_label.setObjectName('whiteLabel')
        main_window.home_action_time_label.setObjectName('smallWhiteLabel')
        main_window.home_action_loading_label.set_flag('working')
        main_window.home_action_loading_label.start_loading()

    def init_connect(self):
        main_window = self.parent()
        main_window.login_button.clicked.connect(self.on_login_button_clicked)
        main_window.logo_button.clicked.connect(self.on_login_button_clicked)

    def on_login_button_clicked(self):
        if self.login_dialog is None:
            self.login_dialog = LoginDialog(self.parent().last_skin_path)
        self.login_dialog.show()
