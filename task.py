# Standard library imports
import os
import re
import time
from functools import partial

# Third-party importss
from PyQt5.QtCore import QObject, pyqtSlot, pyqtSignal, QFileSystemWatcher, QThread

# Application imports
from models import Session, ImportProfile


class Worker(QObject):
    resultReady = pyqtSignal(str)

    def __init__(self):
        super().__init__()

    @pyqtSlot(str)
    def process_file(self, path):
        time.sleep(10)
        self.resultReady.emit(path)


class Task(QObject):
    fileOperate = pyqtSignal(str)

    def __init__(self, parent=None):
        super().__init__(parent)

        self.file_system_watcher = QFileSystemWatcher(self)

        self.file_system_watcher.fileChanged.connect(self.on_file_changed)
        self.file_system_watcher.directoryChanged.connect(self.on_watcher_directory_changed)
        session = Session()
        profiles = session.query(ImportProfile).filter(ImportProfile.watch_folder.isnot(None),
                                                       ImportProfile.enable.is_(True)).all()
        session.close()

        self.worker_thread = QThread()
        self.worker = Worker()
        self.worker.moveToThread(self.worker_thread)
        self.fileOperate.connect(self.worker.process_file)
        self.worker.resultReady.connect(self.on_handle_ready)
        self.worker_thread.start()

        for profile in profiles:
            if not profile.watch_file_regex:
                self.file_system_watcher.addPath(os.path.join(profile.watch_folder, profile.watch_file))
                self.file_system_watcher.addPath(profile.watch_folder)
            else:
                files = os.listdir(profile.watch_folder)
                for file in files:
                    if re.match(profile.watch_file, file):
                        self.file_system_watcher.addPath(os.path.join(profile.watch_folder, file))
                        # self.file_system_watcher.addPath(profile.watch_folder)

    def on_file_changed(self, path):
        file = os.path.basename(path)
        folder = os.path.dirname(path)

        session = Session()
        profiles = session.query(ImportProfile).filter(ImportProfile.watch_folder == folder,
                                                       ImportProfile.enable.is_(True)).all()
        session.close()

        for profile in profiles:
            if not profile.watch_file_regex:
                if file == profile.watch_file:
                    # 处理一下
                    self.fileOperate.emit(path)
            else:
                if re.match(profile.watch_file, file):
                    # 处理一下
                    self.fileOperate.emit(path)

    def on_watcher_directory_changed(self, path):
        # print(path)
        pass

    def on_handle_ready(self, result):
        print(result)

if __name__ == '__main__':

    import pandas as pd

    options = {
        'encoding': 'gb2312',
        'delimiter': chr(0x2c),
        # 'header': None,
        'skiprows': 1,
        'names': ['1', '2', '3', '4', '5'],
        'error_bad_lines': False
    }
    data_frame = pd.read_table('F:/DataCollectionCenter/0001AS_Dev1.DAT', **options)
    print(data_frame.columns.values)