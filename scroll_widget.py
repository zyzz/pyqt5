from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QScrollArea, QVBoxLayout
from PyQt5.QtGui import QPalette, QBrush


class ScrollWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.widget_count = 0
        self.zone_height = 403
        self.scroll_area = QScrollArea()
        self.zone = QWidget()

        self.zone.setAutoFillBackground(True)
        palette = QPalette()
        palette.setBrush(QPalette.Window, QBrush(Qt.white))
        self.zone.setPalette(palette)

        self.scroll_area.setGeometry(-1, -1, 750 + 2, 403)
        self.zone.setGeometry(0, 0, 750, 403)
        self.zone.setWindowFlags(Qt.FramelessWindowHint)
        self.scroll_area.setWidget(self.zone)
        self.scroll_area.horizontalScrollBar().hide()
        self.scroll_area.setAlignment(Qt.AlignLeft)
        self.scroll_area.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)

        layout = QVBoxLayout()
        layout.addWidget(self.scroll_area)
        layout.setSpacing(0)
        layout.setContentsMargins(0, 0, 0, 0)

        self.setLayout(layout)

    def add_scroll_widget(self, widget):
        if self.widget_count == 0:
            widget.move(0, 0)
            self.zone_height = widget.page_height
            self.zone.resize(750, self.zone_height)
        else:
            y = self.zone_height
            self.zone_height += widget.page_height
            if self.zone_height > self.zone.height():
                self.zone.resize(750, self.zone_height)
            widget.move(0, y)

        self.widget_count += 1
