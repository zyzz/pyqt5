from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QLineEdit


class LineEdit(QLineEdit):
    doubleClick = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)

    def mouseDoubleClickEvent(self, event):
        self.doubleClick.emit()
