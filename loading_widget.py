from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QWidget, QLabel, QVBoxLayout, QHBoxLayout
from PyQt5.QtGui import QPixmap


class LoadingWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.account = ''
        self.index = 1

        self.loading_label = QLabel()
        self.account_label = QLabel("Hi,")
        self.waiting_label = QLabel("正在登录，请稍后...")

        pixmap = QPixmap('./resources/loading/{0}'.format(self.index))
        self.loading_label.setFixedSize(pixmap.size())
        self.loading_label.setPixmap(pixmap)
        self.loading_label.setScaledContents(True)

        v_layout = QVBoxLayout()
        v_layout.addWidget(self.account_label)
        v_layout.addWidget(self.waiting_label)
        v_layout.setSpacing(0)
        v_layout.setContentsMargins(0, 15, 0, 15)

        h_layout = QHBoxLayout()
        h_layout.addStretch()
        h_layout.addWidget(self.loading_label)
        h_layout.addLayout(v_layout)
        h_layout.addStretch()
        h_layout.setSpacing(20)
        h_layout.setContentsMargins(0, 0, 0, 30)

        self.timer = QTimer(self)
        self.timer.setInterval(100)
        self.timer.timeout.connect(self.update_pixmap)

        self.setLayout(h_layout)

    def set_account(self, account):
        self.account = account

    def start(self, is_start):
        if is_start:
            self.account_label.setText('Hi, ' + self.account)
            self.timer.start()
        else:
            self.timer.stop()
            self.index = 1
            self.loading_label.setPixmap(QPixmap('./resources/loading/{0}'.format(self.index)))

    def update_pixmap(self):
        self.index += 1
        if self.index > 8:
            self.index = 1
        self.loading_label.setPixmap(QPixmap('./resources/loading/{0}'.format(self.index)))
