import sys
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import pyqtSignal, QTextStream, Qt
from PyQt5.QtNetwork import QLocalSocket, QLocalServer


class QSingleApplication(QApplication):
    messageReceived = pyqtSignal(str)

    def __init__(self, app_id, *argv):

        super(QSingleApplication, self).__init__(*argv)
        self._id = app_id
        self._activation_window = None
        self._activate_on_message = False
        self._server = None

        # Is there another instance running?
        self._out_socket = QLocalSocket()
        self._out_socket.connectToServer(self._id)
        self._out_socket.error.connect(lambda msg: print(msg))
        self._is_running = self._out_socket.waitForConnected()

        if self._out_socket.state() == QLocalSocket.ConnectedState:
            # Yes, there is.
            self._out_stream = QTextStream(self._out_socket)
            self._out_stream.setCodec('UTF-8')
        else:
            # No, there isn't.
            self._out_socket = None
            self._out_stream = None
            self._in_socket = None
            self._in_stream = None
            self._server = QLocalServer()
            self._server.listen(self._id)
            self._server.newConnection.connect(self._on_new_connection)
            self.aboutToQuit.connect(self.remove_server)

    def server(self):
        return self._server

    def is_running(self):
        return self._is_running

    def id(self):
        return self._id

    def activation_window(self):
        return self._activation_window

    def set_activation_window(self, activation_window, activate_on_message=True):
        self._activation_window = activation_window
        self._activate_on_message = activate_on_message

    def activate_window(self):
        if not self._activation_window:
            print("No registered ActivationWindow")
            return
        # Unfortunately this *doesn't* do much of any use, as it won't
        # bring the window to the foreground under KDE... sigh.
        self._activation_window.setWindowState(
            self._activation_window.windowState() & ~Qt.WindowMinimized)
        self._activation_window.raise_()
        self._activation_window.activateWindow()

    def send_message(self, msg, msecs=5000):
        if not self._out_stream:
            return False
        self._out_stream << msg << '\n'
        if not self._out_socket.waitForBytesWritten(msecs):
            raise RuntimeError(
                "Bytes not written within %ss" % (msecs / 1000.))

    def _on_new_connection(self):
        if self._in_socket:
            self._in_socket.readyRead.disconnect(self._on_ready_read)
        self._in_socket = self._server.nextPendingConnection()
        if not self._in_socket:
            return
        self._in_stream = QTextStream(self._in_socket)
        self._in_stream.setCodec('UTF-8')
        self._in_socket.readyRead.connect(self._on_ready_read)
        if self._activate_on_message:
            self.activate_window()

    def _on_ready_read(self):
        while True:
            msg = self._in_stream.readLine()
            if not msg:
                break
            print("Message received")
            self.messageReceived.emit(msg)

    def remove_server(self):
        self._server.close()
        self._server.removeServer(self._id)
