# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'app_wizard_page_txt_additional_options.ui'
#
# Created by: PyQt5 UI code generator 5.9
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(400, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(Form)
        self.verticalLayout.setObjectName("verticalLayout")
        self.wizardTitleLabel = QtWidgets.QLabel(Form)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.wizardTitleLabel.sizePolicy().hasHeightForWidth())
        self.wizardTitleLabel.setSizePolicy(sizePolicy)
        self.wizardTitleLabel.setMinimumSize(QtCore.QSize(0, 50))
        self.wizardTitleLabel.setMidLineWidth(1)
        self.wizardTitleLabel.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.wizardTitleLabel.setObjectName("wizardTitleLabel")
        self.verticalLayout.addWidget(self.wizardTitleLabel)
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setContentsMargins(-1, -1, 0, -1)
        self.formLayout.setObjectName("formLayout")
        self.label_edit_field_line = QtWidgets.QLabel(Form)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_edit_field_line.sizePolicy().hasHeightForWidth())
        self.label_edit_field_line.setSizePolicy(sizePolicy)
        self.label_edit_field_line.setMinimumSize(QtCore.QSize(100, 0))
        self.label_edit_field_line.setObjectName("label_edit_field_line")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label_edit_field_line)
        self.line_edit_field_line = QtWidgets.QLineEdit(Form)
        self.line_edit_field_line.setMaximumSize(QtCore.QSize(120, 16777215))
        self.line_edit_field_line.setObjectName("line_edit_field_line")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.line_edit_field_line)
        self.label_first_data_line = QtWidgets.QLabel(Form)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_first_data_line.sizePolicy().hasHeightForWidth())
        self.label_first_data_line.setSizePolicy(sizePolicy)
        self.label_first_data_line.setMinimumSize(QtCore.QSize(100, 0))
        self.label_first_data_line.setObjectName("label_first_data_line")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_first_data_line)
        self.line_edit_first_data_line = QtWidgets.QLineEdit(Form)
        self.line_edit_first_data_line.setMaximumSize(QtCore.QSize(120, 16777215))
        self.line_edit_first_data_line.setObjectName("line_edit_first_data_line")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.line_edit_first_data_line)
        self.label_last_data_line = QtWidgets.QLabel(Form)
        self.label_last_data_line.setMinimumSize(QtCore.QSize(100, 0))
        self.label_last_data_line.setObjectName("label_last_data_line")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_last_data_line)
        self.line_edit_last_data_line = QtWidgets.QLineEdit(Form)
        self.line_edit_last_data_line.setMaximumSize(QtCore.QSize(120, 16777215))
        self.line_edit_last_data_line.setText("")
        self.line_edit_last_data_line.setObjectName("line_edit_last_data_line")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.line_edit_last_data_line)
        self.verticalLayout.addLayout(self.formLayout)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.wizardTitleLabel.setText(_translate("Form", "你可以为源定义一些附加的选项。"))
        self.label_edit_field_line.setText(_translate("Form", "字段名行："))
        self.line_edit_field_line.setText(_translate("Form", "1"))
        self.label_first_data_line.setText(_translate("Form", "第一个数据行："))
        self.line_edit_first_data_line.setText(_translate("Form", "2"))
        self.label_last_data_line.setText(_translate("Form", "最后一个数据行："))

