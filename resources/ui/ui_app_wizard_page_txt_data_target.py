# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'app_wizard_page_txt_data_target.ui'
#
# Created by: PyQt5 UI code generator 5.9
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(477, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(Form)
        self.verticalLayout.setObjectName("verticalLayout")
        self.wizardTitleLabel = QtWidgets.QLabel(Form)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.wizardTitleLabel.sizePolicy().hasHeightForWidth())
        self.wizardTitleLabel.setSizePolicy(sizePolicy)
        self.wizardTitleLabel.setMinimumSize(QtCore.QSize(0, 50))
        self.wizardTitleLabel.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.wizardTitleLabel.setObjectName("wizardTitleLabel")
        self.verticalLayout.addWidget(self.wizardTitleLabel)
        self.table_widget = QtWidgets.QTableWidget(Form)
        self.table_widget.setAlternatingRowColors(False)
        self.table_widget.setObjectName("table_widget")
        self.table_widget.setColumnCount(3)
        self.table_widget.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        self.table_widget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        self.table_widget.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        self.table_widget.setHorizontalHeaderItem(2, item)
        self.verticalLayout.addWidget(self.table_widget)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.wizardTitleLabel.setText(_translate("Form", "选择目标对象。你可以选择现有的对象，或输入新的对象名。"))
        self.table_widget.setSortingEnabled(True)
        item = self.table_widget.horizontalHeaderItem(0)
        item.setText(_translate("Form", "源表"))
        item = self.table_widget.horizontalHeaderItem(1)
        item.setText(_translate("Form", "目标对象"))
        item = self.table_widget.horizontalHeaderItem(2)
        item.setText(_translate("Form", "新建对象"))

