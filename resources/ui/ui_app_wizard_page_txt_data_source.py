# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'app_wizard_page_txt_data_source.ui'
#
# Created by: PyQt5 UI code generator 5.9
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(400, 300)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(Form)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.wizardTitleLabel = QtWidgets.QLabel(Form)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.wizardTitleLabel.sizePolicy().hasHeightForWidth())
        self.wizardTitleLabel.setSizePolicy(sizePolicy)
        self.wizardTitleLabel.setMinimumSize(QtCore.QSize(0, 50))
        self.wizardTitleLabel.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.wizardTitleLabel.setObjectName("wizardTitleLabel")
        self.verticalLayout_2.addWidget(self.wizardTitleLabel)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_source = QtWidgets.QLabel(Form)
        self.label_source.setObjectName("label_source")
        self.verticalLayout.addWidget(self.label_source)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.line_edit_source = LineEdit(Form)
        self.line_edit_source.setObjectName("line_edit_source")
        self.horizontalLayout.addWidget(self.line_edit_source)
        self.push_button_open_file = QtWidgets.QPushButton(Form)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.push_button_open_file.sizePolicy().hasHeightForWidth())
        self.push_button_open_file.setSizePolicy(sizePolicy)
        self.push_button_open_file.setMinimumSize(QtCore.QSize(0, 0))
        self.push_button_open_file.setMaximumSize(QtCore.QSize(30, 16777215))
        self.push_button_open_file.setObjectName("push_button_open_file")
        self.horizontalLayout.addWidget(self.push_button_open_file)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.label_encoding = QtWidgets.QLabel(Form)
        self.label_encoding.setObjectName("label_encoding")
        self.verticalLayout.addWidget(self.label_encoding)
        self.combo_box_encoding = QtWidgets.QComboBox(Form)
        self.combo_box_encoding.setObjectName("combo_box_encoding")
        self.verticalLayout.addWidget(self.combo_box_encoding)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        spacerItem = QtWidgets.QSpacerItem(20, 128, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem)

        self.retranslateUi(Form)
        self.combo_box_encoding.setCurrentIndex(-1)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.wizardTitleLabel.setText(_translate("Form", "你必须选择一个文件作为数据源。"))
        self.label_source.setText(_translate("Form", "导入从："))
        self.push_button_open_file.setText(_translate("Form", "..."))
        self.label_encoding.setText(_translate("Form", "编码："))

from line_edit import LineEdit
