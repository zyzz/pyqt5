# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'login_widget.ui'
#
# Created by: PyQt5 UI code generator 5.9
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(300, 72)
        Form.setMinimumSize(QtCore.QSize(300, 72))
        Form.setMaximumSize(QtCore.QSize(300, 72))
        self.horizontalLayout = QtWidgets.QHBoxLayout(Form)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setSpacing(11)
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(139, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.logo_label = QtWidgets.QPushButton(Form)
        self.logo_label.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.logo_label.setFocusPolicy(QtCore.Qt.NoFocus)
        self.logo_label.setObjectName("logo_label")
        self.horizontalLayout.addWidget(self.logo_label)
        self.login_button = QtWidgets.QPushButton(Form)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.login_button.sizePolicy().hasHeightForWidth())
        self.login_button.setSizePolicy(sizePolicy)
        self.login_button.setMinimumSize(QtCore.QSize(44, 44))
        self.login_button.setMaximumSize(QtCore.QSize(44, 44))
        self.login_button.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.login_button.setFocusPolicy(QtCore.Qt.NoFocus)
        self.login_button.setText("")
        self.login_button.setFlat(True)
        self.login_button.setObjectName("login_button")
        self.horizontalLayout.addWidget(self.login_button)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.logo_label.setText(_translate("Form", "云平台账号管理"))

