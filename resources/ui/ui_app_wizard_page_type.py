# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'app_wizard_page_type.ui'
#
# Created by: PyQt5 UI code generator 5.9
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(482, 357)
        self.verticalLayout = QtWidgets.QVBoxLayout(Form)
        self.verticalLayout.setObjectName("verticalLayout")
        self.wizardTitleLabel = QtWidgets.QLabel(Form)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.wizardTitleLabel.sizePolicy().hasHeightForWidth())
        self.wizardTitleLabel.setSizePolicy(sizePolicy)
        self.wizardTitleLabel.setMaximumSize(QtCore.QSize(16777215, 50))
        self.wizardTitleLabel.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.wizardTitleLabel.setObjectName("wizardTitleLabel")
        self.verticalLayout.addWidget(self.wizardTitleLabel)
        self.group_box = QtWidgets.QGroupBox(Form)
        self.group_box.setObjectName("group_box")
        self.gridLayout = QtWidgets.QGridLayout(self.group_box)
        self.gridLayout.setObjectName("gridLayout")
        self.radio_button_txt = QtWidgets.QRadioButton(self.group_box)
        self.radio_button_txt.setChecked(True)
        self.radio_button_txt.setObjectName("radio_button_txt")
        self.gridLayout.addWidget(self.radio_button_txt, 0, 0, 1, 1)
        self.radio_button_csv = QtWidgets.QRadioButton(self.group_box)
        self.radio_button_csv.setObjectName("radio_button_csv")
        self.gridLayout.addWidget(self.radio_button_csv, 1, 0, 1, 1)
        self.radio_button_html = QtWidgets.QRadioButton(self.group_box)
        self.radio_button_html.setEnabled(False)
        self.radio_button_html.setObjectName("radio_button_html")
        self.gridLayout.addWidget(self.radio_button_html, 2, 0, 1, 1)
        self.radio_button_xls = QtWidgets.QRadioButton(self.group_box)
        self.radio_button_xls.setEnabled(False)
        self.radio_button_xls.setObjectName("radio_button_xls")
        self.gridLayout.addWidget(self.radio_button_xls, 3, 0, 1, 1)
        self.radio_button_xlsx = QtWidgets.QRadioButton(self.group_box)
        self.radio_button_xlsx.setEnabled(False)
        self.radio_button_xlsx.setObjectName("radio_button_xlsx")
        self.gridLayout.addWidget(self.radio_button_xlsx, 4, 0, 1, 1)
        self.radio_button_json = QtWidgets.QRadioButton(self.group_box)
        self.radio_button_json.setEnabled(False)
        self.radio_button_json.setObjectName("radio_button_json")
        self.gridLayout.addWidget(self.radio_button_json, 5, 0, 1, 1)
        self.verticalLayout.addWidget(self.group_box)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.wizardTitleLabel.setText(_translate("Form", "这个向导允许你指定如何导入数据。你要选择哪种数据导入格式？"))
        self.group_box.setTitle(_translate("Form", "导入类型"))
        self.radio_button_txt.setText(_translate("Form", "文本文件(*.txt)"))
        self.radio_button_csv.setText(_translate("Form", "CSV文件(*.csv)"))
        self.radio_button_html.setText(_translate("Form", "HTML文件(*.htm;*.html)"))
        self.radio_button_xls.setText(_translate("Form", "Excel文件(*.xls)"))
        self.radio_button_xlsx.setText(_translate("Form", "Excel文件(2007或以上版本)(*.xlsx)"))
        self.radio_button_json.setText(_translate("Form", "JSON文件(*.json)"))

