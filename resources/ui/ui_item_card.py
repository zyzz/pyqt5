# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'item_card.ui'
#
# Created by: PyQt5 UI code generator 5.9
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_ItemCard(object):
    def setupUi(self, ItemCard):
        ItemCard.setObjectName("ItemCard")
        ItemCard.resize(132, 87)
        self.base_widget = QtWidgets.QWidget(ItemCard)
        self.base_widget.setGeometry(QtCore.QRect(1, 1, 130, 67))
        self.base_widget.setStyleSheet("QWidget{background:transparent;}")
        self.base_widget.setObjectName("base_widget")
        self.icon = QtWidgets.QLabel(self.base_widget)
        self.icon.setGeometry(QtCore.QRect(0, 0, 130, 67))
        self.icon.setText("")
        self.icon.setObjectName("icon")
        self.using_label = QtWidgets.QLabel(self.base_widget)
        self.using_label.setGeometry(QtCore.QRect(104, 5, 22, 22))
        self.using_label.setStyleSheet("QLabel{background-image:url(\':/choose.png\')}")
        self.using_label.setText("")
        self.using_label.setObjectName("using_label")
        self.delete_widget = QtWidgets.QWidget(ItemCard)
        self.delete_widget.setGeometry(QtCore.QRect(1, 50, 130, 18))
        self.delete_widget.setStyleSheet("QWidget{background:transparent url(:/conceal.png);}")
        self.delete_widget.setObjectName("delete_widget")
        self.delete_btn = QtWidgets.QPushButton(self.delete_widget)
        self.delete_btn.setGeometry(QtCore.QRect(3, 2, 16, 14))
        self.delete_btn.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.delete_btn.setFocusPolicy(QtCore.Qt.NoFocus)
        self.delete_btn.setStyleSheet("QPushButton{background-image:url(\':/remove.png\');border:0px;}")
        self.delete_btn.setText("")
        self.delete_btn.setObjectName("delete_btn")
        self.name = QtWidgets.QLabel(ItemCard)
        self.name.setGeometry(QtCore.QRect(5, 67, 122, 20))
        self.name.setStyleSheet("QLabel{background: transparent; font-size:13px;color:#666666;}")
        self.name.setText("")
        self.name.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft)
        self.name.setObjectName("name")
        self.cover_widget = QtWidgets.QWidget(ItemCard)
        self.cover_widget.setGeometry(QtCore.QRect(0, 0, 132, 69))
        self.cover_widget.setStyleSheet("QWidget{background:transparent url(:/skin-cover.png);}")
        self.cover_widget.setObjectName("cover_widget")

        self.retranslateUi(ItemCard)
        QtCore.QMetaObject.connectSlotsByName(ItemCard)

    def retranslateUi(self, ItemCard):
        _translate = QtCore.QCoreApplication.translate
        ItemCard.setWindowTitle(_translate("ItemCard", "Form"))

import res_rc
