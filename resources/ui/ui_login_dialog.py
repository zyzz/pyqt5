# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'login_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.9
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(400, 320)
        Form.setMinimumSize(QtCore.QSize(320, 320))
        Form.setMaximumSize(QtCore.QSize(400, 320))
        Form.setAutoFillBackground(False)
        Form.setStyleSheet("QDialog{border: 1px solid white;border-radius:1px;background-color: #ffffff;}")
        self.verticalLayout = QtWidgets.QVBoxLayout(Form)
        self.verticalLayout.setContentsMargins(5, 5, 5, 5)
        self.verticalLayout.setObjectName("verticalLayout")
        self.title_bar = TitleBar(Form)
        self.title_bar.setMinimumSize(QtCore.QSize(0, 82))
        self.title_bar.setMaximumSize(QtCore.QSize(16777215, 82))
        self.title_bar.setObjectName("title_bar")
        self.verticalLayout.addWidget(self.title_bar)
        self.stacked_widget = QtWidgets.QStackedWidget(Form)
        self.stacked_widget.setMinimumSize(QtCore.QSize(0, 0))
        self.stacked_widget.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.stacked_widget.setObjectName("stacked_widget")
        self.widget_center = QtWidgets.QWidget()
        self.widget_center.setObjectName("widget_center")
        self.formLayout = QtWidgets.QFormLayout(self.widget_center)
        self.formLayout.setContentsMargins(15, 20, 30, 0)
        self.formLayout.setHorizontalSpacing(10)
        self.formLayout.setVerticalSpacing(15)
        self.formLayout.setObjectName("formLayout")
        self.label_api_root = QtWidgets.QLabel(self.widget_center)
        self.label_api_root.setObjectName("label_api_root")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label_api_root)
        self.combo_box_api_root = QtWidgets.QComboBox(self.widget_center)
        self.combo_box_api_root.setEditable(True)
        self.combo_box_api_root.setObjectName("combo_box_api_root")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.combo_box_api_root)
        self.label_application_id = QtWidgets.QLabel(self.widget_center)
        self.label_application_id.setObjectName("label_application_id")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_application_id)
        self.combo_box_app_id = QtWidgets.QComboBox(self.widget_center)
        self.combo_box_app_id.setEditable(True)
        self.combo_box_app_id.setObjectName("combo_box_app_id")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.combo_box_app_id)
        self.label_rest_api_key = QtWidgets.QLabel(self.widget_center)
        self.label_rest_api_key.setObjectName("label_rest_api_key")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_rest_api_key)
        self.line_edit_rest_api_key = QtWidgets.QLineEdit(self.widget_center)
        self.line_edit_rest_api_key.setEchoMode(QtWidgets.QLineEdit.Password)
        self.line_edit_rest_api_key.setObjectName("line_edit_rest_api_key")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.line_edit_rest_api_key)
        self.label_mater_key = QtWidgets.QLabel(self.widget_center)
        self.label_mater_key.setObjectName("label_mater_key")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.label_mater_key)
        self.line_edit_master_key = QtWidgets.QLineEdit(self.widget_center)
        self.line_edit_master_key.setEchoMode(QtWidgets.QLineEdit.Password)
        self.line_edit_master_key.setObjectName("line_edit_master_key")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.line_edit_master_key)
        self.stacked_widget.addWidget(self.widget_center)
        self.widget_loading = QtWidgets.QWidget()
        self.widget_loading.setObjectName("widget_loading")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.widget_loading)
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        spacerItem = QtWidgets.QSpacerItem(20, 54, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem1)
        self.loading_label = QtWidgets.QLabel(self.widget_loading)
        self.loading_label.setText("")
        self.loading_label.setPixmap(QtGui.QPixmap(":/loginDialog/1"))
        self.loading_label.setScaledContents(True)
        self.loading_label.setObjectName("loading_label")
        self.horizontalLayout_3.addWidget(self.loading_label)
        self.label_tip = QtWidgets.QLabel(self.widget_loading)
        self.label_tip.setObjectName("label_tip")
        self.horizontalLayout_3.addWidget(self.label_tip)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem2)
        self.verticalLayout_3.addLayout(self.horizontalLayout_3)
        spacerItem3 = QtWidgets.QSpacerItem(20, 53, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem3)
        self.stacked_widget.addWidget(self.widget_loading)
        self.verticalLayout.addWidget(self.stacked_widget)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem4)
        self.login_btn = QtWidgets.QPushButton(Form)
        self.login_btn.setMinimumSize(QtCore.QSize(240, 42))
        self.login_btn.setMaximumSize(QtCore.QSize(240, 42))
        self.login_btn.setStyleSheet("width:240px;\n"
"height:42px; \n"
"background:rgba(13,215,103,1);\n"
"border-radius: 21px ; \n"
"font-size:16px;\n"
"font-family:微软雅黑;\n"
"color:rgba(255,255,255,1);\n"
"line-height:26px;")
        self.login_btn.setObjectName("login_btn")
        self.horizontalLayout.addWidget(self.login_btn)
        spacerItem5 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem5)
        self.verticalLayout.addLayout(self.horizontalLayout)
        spacerItem6 = QtWidgets.QSpacerItem(20, 18, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem6)

        self.retranslateUi(Form)
        self.stacked_widget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.label_api_root.setText(_translate("Form", "服务地址："))
        self.label_application_id.setText(_translate("Form", "应用ID："))
        self.label_rest_api_key.setText(_translate("Form", "应用秘钥："))
        self.label_mater_key.setText(_translate("Form", "超级秘钥："))
        self.label_tip.setText(_translate("Form", "正在登录，请稍后..."))
        self.login_btn.setText(_translate("Form", "登录"))

from title_bar import TitleBar
import res_rc
