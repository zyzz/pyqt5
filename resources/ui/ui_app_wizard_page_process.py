# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'app_wizard_page_process.ui'
#
# Created by: PyQt5 UI code generator 5.9
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(635, 456)
        self.verticalLayout = QtWidgets.QVBoxLayout(Form)
        self.verticalLayout.setObjectName("verticalLayout")
        self.wizardTitleLabel = QtWidgets.QLabel(Form)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.wizardTitleLabel.sizePolicy().hasHeightForWidth())
        self.wizardTitleLabel.setSizePolicy(sizePolicy)
        self.wizardTitleLabel.setMinimumSize(QtCore.QSize(0, 50))
        self.wizardTitleLabel.setMaximumSize(QtCore.QSize(16777215, 50))
        self.wizardTitleLabel.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.wizardTitleLabel.setObjectName("wizardTitleLabel")
        self.verticalLayout.addWidget(self.wizardTitleLabel)
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label_table = QtWidgets.QLabel(Form)
        self.label_table.setObjectName("label_table")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label_table)
        self.label_table_value = QtWidgets.QLabel(Form)
        self.label_table_value.setText("")
        self.label_table_value.setObjectName("label_table_value")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.label_table_value)
        self.label_processed = QtWidgets.QLabel(Form)
        self.label_processed.setObjectName("label_processed")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_processed)
        self.label_processed_value = QtWidgets.QLabel(Form)
        self.label_processed_value.setText("")
        self.label_processed_value.setObjectName("label_processed_value")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.label_processed_value)
        self.label_error = QtWidgets.QLabel(Form)
        self.label_error.setObjectName("label_error")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_error)
        self.label_error_value = QtWidgets.QLabel(Form)
        self.label_error_value.setText("")
        self.label_error_value.setObjectName("label_error_value")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.label_error_value)
        self.label_added = QtWidgets.QLabel(Form)
        self.label_added.setObjectName("label_added")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.label_added)
        self.label_added_value = QtWidgets.QLabel(Form)
        self.label_added_value.setText("")
        self.label_added_value.setObjectName("label_added_value")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.label_added_value)
        self.label_updated = QtWidgets.QLabel(Form)
        self.label_updated.setObjectName("label_updated")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.label_updated)
        self.label_updated_value = QtWidgets.QLabel(Form)
        self.label_updated_value.setText("")
        self.label_updated_value.setObjectName("label_updated_value")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.label_updated_value)
        self.label_deleted = QtWidgets.QLabel(Form)
        self.label_deleted.setObjectName("label_deleted")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.LabelRole, self.label_deleted)
        self.label_deleted_value = QtWidgets.QLabel(Form)
        self.label_deleted_value.setText("")
        self.label_deleted_value.setObjectName("label_deleted_value")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.FieldRole, self.label_deleted_value)
        self.label_time = QtWidgets.QLabel(Form)
        self.label_time.setObjectName("label_time")
        self.formLayout.setWidget(6, QtWidgets.QFormLayout.LabelRole, self.label_time)
        self.label_time_value = QtWidgets.QLabel(Form)
        self.label_time_value.setText("")
        self.label_time_value.setObjectName("label_time_value")
        self.formLayout.setWidget(6, QtWidgets.QFormLayout.FieldRole, self.label_time_value)
        self.verticalLayout.addLayout(self.formLayout)
        self.text_edit = QtWidgets.QTextEdit(Form)
        self.text_edit.setReadOnly(True)
        self.text_edit.setObjectName("text_edit")
        self.verticalLayout.addWidget(self.text_edit)
        self.progress_bar = QtWidgets.QProgressBar(Form)
        self.progress_bar.setProperty("value", 0)
        self.progress_bar.setObjectName("progress_bar")
        self.verticalLayout.addWidget(self.progress_bar)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.wizardTitleLabel.setText(_translate("Form", "我们已收集向导导入数据时所需的全部信息。点击[开始]按钮进行导入。"))
        self.label_table.setText(_translate("Form", "表："))
        self.label_processed.setText(_translate("Form", "已处理："))
        self.label_error.setText(_translate("Form", "错误："))
        self.label_added.setText(_translate("Form", "已添加："))
        self.label_updated.setText(_translate("Form", "已更新："))
        self.label_deleted.setText(_translate("Form", "已删除："))
        self.label_time.setText(_translate("Form", "时间："))
        self.text_edit.setHtml(_translate("Form", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'SimSun\'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))

import res_rc
