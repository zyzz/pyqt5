# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'app_wizard_page_mode.ui'
#
# Created by: PyQt5 UI code generator 5.9
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(522, 369)
        self.verticalLayout = QtWidgets.QVBoxLayout(Form)
        self.verticalLayout.setObjectName("verticalLayout")
        self.wizardTitleLabel = QtWidgets.QLabel(Form)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.wizardTitleLabel.sizePolicy().hasHeightForWidth())
        self.wizardTitleLabel.setSizePolicy(sizePolicy)
        self.wizardTitleLabel.setMaximumSize(QtCore.QSize(16777215, 50))
        self.wizardTitleLabel.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.wizardTitleLabel.setObjectName("wizardTitleLabel")
        self.verticalLayout.addWidget(self.wizardTitleLabel)
        self.group_box = QtWidgets.QGroupBox(Form)
        self.group_box.setObjectName("group_box")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.group_box)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.vertical_layout_radio_button = QtWidgets.QVBoxLayout()
        self.vertical_layout_radio_button.setObjectName("vertical_layout_radio_button")
        self.radio_button_add = QtWidgets.QRadioButton(self.group_box)
        self.radio_button_add.setChecked(True)
        self.radio_button_add.setObjectName("radio_button_add")
        self.vertical_layout_radio_button.addWidget(self.radio_button_add)
        self.radio_button_update = QtWidgets.QRadioButton(self.group_box)
        self.radio_button_update.setObjectName("radio_button_update")
        self.vertical_layout_radio_button.addWidget(self.radio_button_update)
        self.radio_button_add_or_update = QtWidgets.QRadioButton(self.group_box)
        self.radio_button_add_or_update.setEnabled(False)
        self.radio_button_add_or_update.setObjectName("radio_button_add_or_update")
        self.vertical_layout_radio_button.addWidget(self.radio_button_add_or_update)
        self.radio_button_delete = QtWidgets.QRadioButton(self.group_box)
        self.radio_button_delete.setEnabled(False)
        self.radio_button_delete.setObjectName("radio_button_delete")
        self.vertical_layout_radio_button.addWidget(self.radio_button_delete)
        self.radio_button_copy = QtWidgets.QRadioButton(self.group_box)
        self.radio_button_copy.setEnabled(False)
        self.radio_button_copy.setObjectName("radio_button_copy")
        self.vertical_layout_radio_button.addWidget(self.radio_button_copy)
        self.verticalLayout_2.addLayout(self.vertical_layout_radio_button)
        self.verticalLayout.addWidget(self.group_box)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.wizardTitleLabel.setText(_translate("Form", "请选择一个所需的导入模式。"))
        self.group_box.setTitle(_translate("Form", "导入模式"))
        self.radio_button_add.setText(_translate("Form", "添加：添加记录到目标对象"))
        self.radio_button_update.setText(_translate("Form", "更新：更新目标和源记录相符的记录"))
        self.radio_button_add_or_update.setText(_translate("Form", "添加或更新：如果目标存在相同记录，更新它。否则，添加它"))
        self.radio_button_delete.setText(_translate("Form", "删除：删除目标中的源记录相符的内容"))
        self.radio_button_copy.setText(_translate("Form", "复制：删除目标全部记录，并从源重新导入"))

