# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'app_wizard_page_server.ui'
#
# Created by: PyQt5 UI code generator 5.9
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(656, 519)
        self.verticalLayout = QtWidgets.QVBoxLayout(Form)
        self.verticalLayout.setObjectName("verticalLayout")
        self.wizardTitleLabel = QtWidgets.QLabel(Form)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.wizardTitleLabel.sizePolicy().hasHeightForWidth())
        self.wizardTitleLabel.setSizePolicy(sizePolicy)
        self.wizardTitleLabel.setMaximumSize(QtCore.QSize(16777215, 50))
        self.wizardTitleLabel.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.wizardTitleLabel.setObjectName("wizardTitleLabel")
        self.verticalLayout.addWidget(self.wizardTitleLabel)
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setContentsMargins(20, 15, 40, -1)
        self.formLayout.setHorizontalSpacing(10)
        self.formLayout.setVerticalSpacing(20)
        self.formLayout.setObjectName("formLayout")
        self.server_addr_label = QtWidgets.QLabel(Form)
        self.server_addr_label.setObjectName("server_addr_label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.server_addr_label)
        self.server_addr_combo_box = QtWidgets.QComboBox(Form)
        self.server_addr_combo_box.setObjectName("server_addr_combo_box")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.server_addr_combo_box)
        self.app_id_label = QtWidgets.QLabel(Form)
        self.app_id_label.setObjectName("app_id_label")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.app_id_label)
        self.app_id_combo_box = QtWidgets.QComboBox(Form)
        self.app_id_combo_box.setObjectName("app_id_combo_box")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.app_id_combo_box)
        self.verticalLayout.addLayout(self.formLayout)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.wizardTitleLabel.setText(_translate("Form", "请选择一个要导入的服务器实例。"))
        self.server_addr_label.setText(_translate("Form", "服务器地址："))
        self.app_id_label.setText(_translate("Form", "应用名称："))

