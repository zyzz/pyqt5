# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'skin_center.ui'
#
# Created by: PyQt5 UI code generator 5.9
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(452, 350)
        Form.setStyleSheet("QDialog{border: 1px solid white;border-radius:1px;background-color: #ffffff;}")
        self.base_widget = QtWidgets.QWidget(Form)
        self.base_widget.setGeometry(QtCore.QRect(5, 5, 442, 82))
        self.base_widget.setAutoFillBackground(True)
        self.base_widget.setObjectName("base_widget")
        self.close_btn = SystemButton(self.base_widget)
        self.close_btn.setGeometry(QtCore.QRect(406, 0, 36, 36))
        self.close_btn.setFocusPolicy(QtCore.Qt.NoFocus)
        self.close_btn.setText("")
        self.close_btn.setObjectName("close_btn")
        self.label = QtWidgets.QLabel(self.base_widget)
        self.label.setGeometry(QtCore.QRect(71, 0, 300, 30))
        self.label.setStyleSheet("QLabel{color:#ffffff;font-family: 方正黑体_GBK;font-size: 12px;text-align: center;font-weight:bold;}")
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.sys_btn = QtWidgets.QPushButton(self.base_widget)
        self.sys_btn.setGeometry(QtCore.QRect(10, 50, 60, 24))
        self.sys_btn.setFocusPolicy(QtCore.Qt.NoFocus)
        self.sys_btn.setStyleSheet("QPushButton{background:transparent;text-align:center;font-family: 方正黑体_GBK;font-size:14px;color:#ffffff;}")
        self.sys_btn.setObjectName("sys_btn")
        self.custom_btn = QtWidgets.QPushButton(self.base_widget)
        self.custom_btn.setGeometry(QtCore.QRect(75, 50, 60, 24))
        self.custom_btn.setFocusPolicy(QtCore.Qt.NoFocus)
        self.custom_btn.setStyleSheet("QPushButton{background:transparent;text-align:center;font-family: 方正黑体_GBK;font-size:14px;color:#ffffff;}")
        self.custom_btn.setObjectName("custom_btn")
        self.indicator = QtWidgets.QLabel(self.base_widget)
        self.indicator.setGeometry(QtCore.QRect(10, 75, 60, 2))
        self.indicator.setStyleSheet("QLabel{background-image:url(\':/underline.png\');background-position:center;}")
        self.indicator.setText("")
        self.indicator.setObjectName("indicator")
        self.skin_widget = QtWidgets.QWidget(Form)
        self.skin_widget.setGeometry(QtCore.QRect(20, 98, 420, 240))
        self.skin_widget.setObjectName("skin_widget")

        self.retranslateUi(Form)
        self.close_btn.clicked.connect(Form.close)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.label.setText(_translate("Form", "皮肤设置"))
        self.sys_btn.setText(_translate("Form", "默认皮肤"))
        self.custom_btn.setText(_translate("Form", "自定义"))

from system_button import SystemButton
import res_rc
