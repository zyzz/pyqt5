import os

for name in os.listdir(os.path.dirname(__file__)):
    name, ext = os.path.splitext(name)
    if ext == '.ui':
        os.system(r'pyuic5 -o ui_{name}.py {name}.ui '.format(name=name))
