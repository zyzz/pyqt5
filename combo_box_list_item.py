from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QHBoxLayout
from PyQt5.QtGui import QIcon, QPixmap, QCursor


class ComboBoxListItem(QWidget):
    showItem = pyqtSignal(str)
    removeItem = pyqtSignal(str)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.mouse_press = False

        self.text_label = QLabel()
        self.delete_button = QPushButton()
        pixmap = QPixmap(':/loginDialog/login/delete.png')
        self.delete_button.setIcon(QIcon(pixmap))
        self.delete_button.setIconSize(pixmap.size())
        self.delete_button.setFocusPolicy(Qt.NoFocus)
        self.delete_button.setCursor(QCursor(Qt.PointingHandCursor))
        self.delete_button.setObjectName('transparentButton')
        self.delete_button.clicked.connect(lambda: self.removeItem.emit(self.text_label.text()))

        main_layout = QHBoxLayout()
        main_layout.addWidget(self.text_label)
        main_layout.addStretch()
        main_layout.addWidget(self.delete_button)
        main_layout.setContentsMargins(5, 5, 5, 5)
        main_layout.setSpacing(5)
        self.setLayout(main_layout)

    def set_text(self, text):
        self.text_label.setText(text)

    def get_text(self):
        return self.text_label.text()

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.mouse_press = True

    def mouseReleaseEvent(self, event):
        if self.mouse_press and self.rect().contains(event.pos()):
            self.showItem.emit(self.text_label.text())
        self.mouse_press = False
