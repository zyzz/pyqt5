import time

# from blinker import signal
from sqlalchemy import distinct
from PyQt5.QtCore import QThread, pyqtSignal, Qt, QTimer
from PyQt5.QtWidgets import QListWidget, QListWidgetItem, QDialog, QCompleter
from PyQt5.QtGui import QPixmap, QStandardItem, QStandardItemModel

import utils
from models import Session, ServerProfile
from draggable_shadow_widget import DraggableShadowWidget
from resources.ui import ui_login_dialog
from combo_box_list_item import ComboBoxListItem
from msg_box import MsgBox


class LoginThread(QThread):
    loginSuccess = pyqtSignal()
    loginFailed = pyqtSignal()

    def __init__(self):
        super().__init__()

    def run(self):
        time.sleep(3)
        self.loginSuccess.emit()
        return


class LoginDialog(DraggableShadowWidget):
    def __init__(self, skin, parent=None):
        super().__init__(parent)
        self.ui = ui_login_dialog.Ui_Form()
        self.ui.setupUi(self)

        self.ui.label_api_root.setObjectName('loginLabel')
        self.ui.label_application_id.setObjectName('loginLabel')
        self.ui.label_rest_api_key.setObjectName('loginLabel')
        self.ui.label_mater_key.setObjectName('loginLabel')

        self.login_thread = LoginThread()

        # self.ui.login_btn.setObjectName('whiteButton')
        self.skin = skin
        self.reset_title_skin(skin)
        self.ui.title_bar.set_title_name('账号管理')
        self.ui.title_bar.set_title_width(self.width() - utils.SHADOW_WIDTH * 2)
        self.ui.title_bar.closeClicked.connect(lambda: self.close())

        self.ui.login_btn.clicked.connect(self.verify)

        self.api_root_list_widget = QListWidget()
        self.api_root_list_widget.setObjectName('comboBoxListWidget')
        self.ui.combo_box_api_root.setModel(self.api_root_list_widget.model())
        self.ui.combo_box_api_root.setView(self.api_root_list_widget)
        self.ui.combo_box_api_root.currentTextChanged.connect(self.on_api_root_changed)

        self.app_id_list_widget = QListWidget()
        self.app_id_list_widget.setObjectName('comboBoxListWidget')
        self.ui.combo_box_app_id.setModel(self.app_id_list_widget.model())
        self.ui.combo_box_app_id.setView(self.app_id_list_widget)
        self.ui.combo_box_app_id.currentTextChanged.connect(self.on_app_id_changed)

        # loading_widget
        self.index = 1

        self.timer = QTimer(self)
        self.timer.setInterval(100)
        self.timer.timeout.connect(self.update_pixmap)
        self.init_data()

    def init_data(self):
        self.clear_data()

        session = Session()
        for result in session.query(distinct(ServerProfile.api_root)).all():
            api_root_item = ComboBoxListItem()
            api_root_item.set_text(result[0])
            api_root_item.showItem.connect(self.on_show_api_root)
            api_root_item.removeItem.connect(self.on_remove_api_root)
            item = QListWidgetItem(self.api_root_list_widget)
            self.api_root_list_widget.setItemWidget(item, api_root_item)
        session.close()

        '''
        model = QStandardItemModel()
        blank_item = QStandardItem('')
        model.appendRow(blank_item)

        completer = QCompleter(self.ui.combo_box_api_root)
        completer.setCaseSensitivity(Qt.CaseInsensitive)
        completer.setModel(model)
        completer.setCompletionColumn(self.ui.combo_box_api_root.modelColumn())
        completer.setCompletionMode(QCompleter.UnfilteredPopupCompletion)
        completer.setMaxVisibleItems(10)
        completer.popup().setStyleSheet(
            "QListView{font:75 12pt \"微软雅黑\";subcontrol-origin: padding;subcontrol-position: top right;width: 20px;}")
        self.ui.combo_box_api_root.setCompleter(completer)
        '''

        '''
        for i in range(3):
            app_id_item = ComboBoxListItem()
            app_id_item.set_text('app{0}'.format(i))
            app_id_item.showItem.connect(self.on_show_app_id)
            app_id_item.removeItem.connect(self.on_remove_app_id)
            item = QListWidgetItem(self.app_id_list_widget)
            self.app_id_list_widget.setItemWidget(item, app_id_item)
        '''

    def clear_data(self):
        self.ui.combo_box_api_root.clear()
        self.ui.combo_box_api_root.setCurrentText('')
        self.ui.combo_box_app_id.clear()
        self.ui.combo_box_app_id.setCurrentText('')
        self.ui.line_edit_rest_api_key.clear()
        self.ui.line_edit_master_key.clear()

    def on_api_root_changed(self, api_root):
        self.ui.combo_box_app_id.clear()
        session = Session()
        for result in session.query(ServerProfile.application_id).filter_by(api_root=api_root).all():
            app_id_item = ComboBoxListItem()
            app_id_item.set_text(result[0])
            app_id_item.showItem.connect(self.on_show_app_id)
            app_id_item.removeItem.connect(self.on_remove_app_id)
            item = QListWidgetItem(self.app_id_list_widget)
            self.app_id_list_widget.setItemWidget(item, app_id_item)
        session.close()

    def on_app_id_changed(self, app_id):
        self.ui.line_edit_rest_api_key.clear()
        self.ui.line_edit_master_key.clear()

        api_root = self.ui.combo_box_api_root.currentText()
        session = Session()
        server_profile = session.query(ServerProfile).filter_by(api_root=api_root, application_id=app_id).first()
        if server_profile is not None:
            self.ui.line_edit_rest_api_key.setText(server_profile.rest_api_key)
            self.ui.line_edit_master_key.setText(server_profile.master_key)
        session.close()

    def on_show_api_root(self, api_root):
        self.ui.combo_box_api_root.setEditText(api_root)
        self.ui.combo_box_api_root.hidePopup()

    def on_remove_api_root(self, api_root):
        msg_box = MsgBox(self.skin)
        msg_box.set_info('删除服务器', '您确定删除此服务器的所有信息？', QPixmap(':/loginDialog/login/attention.png'), False)
        if msg_box.exec_() == QDialog.Accepted:
            for index in range(self.api_root_list_widget.count()):
                item = self.api_root_list_widget.item(index)
                api_root_item = self.api_root_list_widget.itemWidget(item)
                if api_root == api_root_item.get_text():
                    self.api_root_list_widget.takeItem(index)
                    session = Session()
                    [session.delete(server_profile) for server_profile in
                     session.query(ServerProfile).filter_by(api_root=api_root).all()]
                    session.commit()
                    session.close()
                    break
            self.init_data()
        self.ui.combo_box_api_root.hidePopup()

    def on_show_app_id(self, api_root):
        self.ui.combo_box_app_id.setEditText(api_root)
        self.ui.combo_box_app_id.hidePopup()

    def on_remove_app_id(self, app_id):
        msg_box = MsgBox(self.skin)
        msg_box.set_info('删除应用ID', '您确定删除此应用ID的所有信息？', QPixmap(':/loginDialog/login/attention.png'), False)
        if msg_box.exec_() == QDialog.Accepted:
            api_root = self.ui.combo_box_api_root.currentText()
            session = Session()
            for index in range(self.app_id_list_widget.count()):
                item = self.app_id_list_widget.item(index)
                app_id_item = self.app_id_list_widget.itemWidget(item)
                if app_id == app_id_item.get_text():
                    self.app_id_list_widget.takeItem(index)
                    server_profile = session.query(ServerProfile).filter_by(api_root=api_root,
                                                                            application_id=app_id).first()
                    if server_profile is not None:
                        session.delete(server_profile)
                        session.commit()
                    break
            if session.query(ServerProfile).filter_by(api_root=api_root).first() is None:
                self.init_data()
            session.close()
        self.ui.combo_box_api_root.hidePopup()

    def verify(self):
        current_widget = self.ui.stacked_widget.currentWidget()

        # account = self.account_line_edit.text()
        # password = self.password_line_edit.text()

        if current_widget != self.ui.widget_loading:
            api_root = self.ui.combo_box_api_root.currentText()
            app_id = self.ui.combo_box_app_id.currentText()
            rest_api_key = self.ui.line_edit_rest_api_key.text()
            master_key = self.ui.line_edit_master_key.text()

            if not api_root:
                msg_box = MsgBox(self.skin)
                msg_box.set_info('提示', '服务地址不能为空！', QPixmap(':/loginDialog/login/attention.png'), True)
                msg_box.exec_()
                self.ui.combo_box_api_root.setFocus()
                return

            if not app_id:
                msg_box = MsgBox(self.skin)
                msg_box.set_info('提示', '应用ID不能为空！', QPixmap(':/loginDialog/login/attention.png'), True)
                msg_box.exec_()
                self.ui.combo_box_app_id.setFocus()
                return

            if not rest_api_key:
                msg_box = MsgBox(self.skin)
                msg_box.set_info('提示', '应用秘钥不能为空！', QPixmap(':/loginDialog/login/attention.png'), True)
                msg_box.exec_()
                self.ui.line_edit_rest_api_key.setFocus()
                return

            self.ui.login_btn.setText("取消")
            self.ui.stacked_widget.setCurrentWidget(self.ui.widget_loading)
            self.start(True)

            self.login_thread.loginFailed.connect(self.on_login_failed)
            self.login_thread.loginSuccess.connect(self.on_login_success)
            self.login_thread.start()

        else:

            self.login_thread.loginFailed.disconnect(self.on_login_failed)
            self.login_thread.loginSuccess.disconnect(self.on_login_success)

            self.ui.login_btn.setText('登录')
            self.start(False)
            self.ui.stacked_widget.setCurrentWidget(self.ui.widget_center)

    def start(self, is_start):
        if is_start:
            self.timer.start()
        else:
            self.timer.stop()
            self.index = 1
            self.ui.loading_label.setPixmap(QPixmap(':/loginDialog/login/{0}.png'.format(self.index)))

    def update_pixmap(self):
        self.index += 1
        if self.index > 8:
            self.index = 1
        self.ui.loading_label.setPixmap(QPixmap(':/loginDialog/login/{0}.png'.format(self.index)))

    def on_login_success(self):
        api_root = self.ui.combo_box_api_root.currentText()
        app_id = self.ui.combo_box_app_id.currentText()
        rest_api_key = self.ui.line_edit_rest_api_key.text()
        master_key = self.ui.line_edit_master_key.text()

        session = Session()
        server_profile = session.query(ServerProfile).filter_by(api_root=api_root, application_id=app_id).first()
        if server_profile is not None:
            server_profile.rest_api_key = rest_api_key
            server_profile.master_key = master_key
        else:
            server_profile = ServerProfile(api_root=api_root, application_id=app_id, rest_api_key=rest_api_key,
                                           master_key=master_key)
            session.add(server_profile)
        session.commit()
        session.close()

        if '取消' == self.ui.login_btn.text():
            self.verify()
        self.init_data()

    def on_login_failed(self):
        if '取消' == self.ui.login_btn.text():
            self.verify()

    def reset_title_skin(self, skin):
        self.skin = skin
        self.ui.title_bar.set_title_background(self.skin)


if __name__ == '__main__':
    import sys

    from PyQt5.QtCore import QFile
    from PyQt5.QtWidgets import QApplication, qApp

    app = QApplication(sys.argv)

    qss = QFile(':/qssfile/qss/mmap.qss')
    qss.open(QFile.ReadOnly)
    style = bytes(qss.readAll()).decode('utf-8')
    qApp.setStyleSheet(style)
    qss.close()

    login_dialog = LoginDialog(':/background/skin/1.png')
    sys.exit(login_dialog.exec_())
