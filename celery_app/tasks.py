from functools import wraps

import requests
from celery_app.app import app
from parse_rest import connection, datatypes


# from parse_rest.datatypes import Object


def vertify_parse_rest(f):
    @wraps(f)
    def decorated_function(request, fields, datas, **kwargs):
        connection.API_ROOT = request['api_root']
        connection.register(request['app_id'], request['rest_key'], master_key=request['master_key'])

        datatypes.API_ROOT = connection.API_ROOT
        cls = datatypes.Object.factory(request['class_name'])
        batcher = connection.ParseBatcher()
        batcher.__class__.ENDPOINT_ROOT = '/'.join((connection.API_ROOT, 'batch'))
        return f(batcher, cls, fields, datas, **kwargs)

    return decorated_function


@app.task
@vertify_parse_rest
def create_objects(batcher, cls, fields, datas, **kwargs):
    batcher.batch_save([cls(**data) for data in datas])


@app.task
@vertify_parse_rest
def update_objects(batcher, cls, fields, datas, **kwargs):
    results = query_objects(cls, fields, datas, update=True)
    batcher.batch_save(results)


@app.task
@vertify_parse_rest
def delete_objects(batcher, cls, fields, datas, **kwargs):
    results = query_objects(cls, fields, datas)
    batcher.batch_delete(results)


def query_objects(cls, fields, datas, update=False):
    primary_keys = [field['target_field_name'] for field in fields if field['target_is_key']]
    ret = []
    for data in datas:
        f = next({k: v} for k, v in data.items() if k in primary_keys)
        querysets = cls().Query.filter(**f)
        for result in querysets:
            if not update:
                ret.append(result)
                break
            for key, value in data.items():
                setattr(result, key, datatypes.ParseType.convert_from_parse(key, value))
            ret.append(result)

    return ret
