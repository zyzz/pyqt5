import time
import json
from urllib.parse import urljoin

from PyQt5.QtCore import QObject, pyqtSignal, pyqtProperty, QTimer, QUrl, QUrlQuery
from PyQt5.QtNetwork import QNetworkAccessManager, QNetworkRequest


class MonitorWorker(QObject):
    stateChanged = pyqtSignal(str)
    activeChanged = pyqtSignal(int)
    processedChanged = pyqtSignal(int)


    def __init__(self):
        super().__init__()
        self.server_address = ''
        self.dashboard_address = ''
        self._state = ''
        self.set_server_address('http://localhost:5555')
        self.manager = QNetworkAccessManager(self)
        # self.manager.finished.connect(self.reply_finished)
        self.on_timeout()
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.on_timeout)
        self.timer.start(1000)

    @pyqtProperty(str)
    def state(self):
        return self._state

    @state.setter
    def state(self, state):
        if state != self._state:
            self._state = state
            self.stateChanged.emit(state)

    def set_server_address(self, addr):
        self.server_address = addr

        self.dashboard_address = urljoin(self.server_address, 'dashboard')

    def on_timeout(self):
        request = QNetworkRequest()
        url_query = QUrlQuery()
        url_query.addQueryItem('json', '1')
        url_query.addQueryItem('_', str(int(time.time() * 1000)))
        url = QUrl(self.dashboard_address)
        url.setQuery(url_query)
        request.setUrl(url)
        reply = self.manager.get(request)
        reply.readyRead.connect(self.on_ready_read)
        reply.error.connect(self.on_network_error)

        '''
        try:
            response = requests.get(self.dashboard_address, params={'json': 1, '_': int(time.time() * 1000)})
        except Exception as e:
            self.state = '已离线'
            return

        if response.status_code == 200:
            self.state = '已连接'
            print(response.content)
        else:
            self.state = '已离线'
        '''

    def on_ready_read(self):
        reply = self.sender()
        response = json.loads(bytes(reply.readAll()).decode())
        data = response.get('data', [])
        if not data:
            self.state = '暂无数据'
            return
        if not data[0]['status']:
            self.state = '已离线'
            return
        self.state = '已连接'

    def on_network_error(self, code):
        self.state = '已断开'
