from PyQt5.QtCore import QObject


class UtilitiesPage(QObject):
    def __init__(self, parent=None):
        super().__init__(parent)

    def init_ui(self):
        main_window = self.parent()
        main_window.utilities_action.setObjectName('transparentWidget')
        main_window.utilities_action_tip_label.setObjectName('whiteLabel')

    def init_connect(self):
        pass
