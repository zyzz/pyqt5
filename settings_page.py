from PyQt5.QtCore import QObject


class SettingsPage(QObject):
    def __init__(self, parent=None):
        super().__init__(parent)

    def init_ui(self):
        main_window = self.parent()
        main_window.settings_action.setObjectName('transparentWidget')
        main_window.settings_action_tip_label.setObjectName('whiteLabel')
        main_window.settings_database_button.setObjectName('settingButton')
        setting_buttons = [
            main_window.settings_database_button,
            main_window.settings_cache_button,
            main_window.settings_queue_button,
            main_window.settings_file_button,
        ]

        for btn in setting_buttons:
            btn.setObjectName('settingButton')
            btn.setStyleSheet('')

    def init_connect(self):
        pass
