import copy
from functools import partial

from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QToolButton
from PyQt5.QtGui import QPalette, QColor, QIcon, QPixmap


class ToolWidget(QWidget):
    turnCurrentPage = pyqtSignal(int)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.main_window = parent

        palette = QPalette()
        palette.setColor(QPalette.Background, QColor(233, 238, 241))
        self.setPalette(palette)

    def switch_selected_page_index(self, index):
        self.turnCurrentPage.emit(index)
        return
        for k, v in self.tool_button_icon.items():
            icon = QIcon()
            icon.addPixmap(v[QIcon.Normal], QIcon.Normal, QIcon.On)
            icon.addPixmap(v[QIcon.Active], QIcon.Active, QIcon.On)
            if k == self.sender():
                icon.addPixmap(v[QIcon.Selected], QIcon.Normal, QIcon.On)
                icon.addPixmap(v[QIcon.Selected], QIcon.Active, QIcon.On)
                self.turnCurrentPage.emit(k.index)
            k.setIcon(icon)

    def init_ui(self):
        buttons = [
            self.main_window.run_state_tool_btn,
            self.main_window.app_manage_tool_btn,
            self.main_window.romote_monitor_tool_btn,
            self.main_window.settings_center_tool_btn,
            self.main_window.utilities_tool_btn
        ]

        for index, tool_btn in enumerate(buttons):
            tool_btn.setObjectName('transparentToolButton')
            tool_btn.clicked.connect(partial(self.switch_selected_page_index, index))

    def init_connect(self):
        self.turnCurrentPage.connect(self.main_window.set_current_page_index)


if __name__ == '__main__':
    import sys
    from PyQt5.QtWidgets import QApplication

    app = QApplication(sys.argv)
    tool_widget = ToolWidget()
    tool_widget.init_connect()
    tool_widget.show()
    sys.exit(app.exec_())
