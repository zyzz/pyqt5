from PyQt5.QtWidgets import QWidget

from resources.ui import ui_login_widget
from login_dialog import LoginDialog


class LoginWidget(QWidget):
    def __init__(self, skin, parent=None):
        super().__init__(parent)
        self.ui = ui_login_widget.Ui_Form()
        self.ui.setupUi(self)
        self.ui.login_button.setObjectName('loginButton')
        self.ui.logo_label.setObjectName('whiteButton')

        self.last_skin_path = skin

        self.login_dialog = None

    def init_connect(self):
        self.ui.logo_label.clicked(self.on_login_button_clicked)
        self.ui.login_button.clicked(self.on_login_button_clicked)

    def on_login_button_clicked(self):
        if self.login_dialog is None:
            self.login_dialog = LoginDialog(self.last_skin_path)
        self.login_dialog.show()

    def on_logo_label_clicked(self):
        if self.login_dialog is None:
            self.login_dialog = LoginDialog(self.last_skin_path)
        self.login_dialog.show()

    def reset_skin(self, skin):
        self.last_skin_path = skin
        if self.login_dialog is not None:
            self.login_dialog.reset_title_skin(self.last_skin_path)
