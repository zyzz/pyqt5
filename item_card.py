import os

from PyQt5.QtCore import Qt, pyqtSignal, QRect, QStandardPaths, QEvent
from PyQt5.QtWidgets import QWidget
from PyQt5.QtGui import QPixmap

from resources.ui import ui_item_card


class ItemCard(QWidget):
    sendBackgroundName = pyqtSignal(str)
    sendEnterBackground = pyqtSignal(str)
    sendLeaveBackground = pyqtSignal()
    sendDeleteSignal = pyqtSignal(str)
    sendAddSignal = pyqtSignal()

    def __init__(self, title='', flag=False, parent=None):
        super().__init__(parent)
        self.ui = ui_item_card.Ui_ItemCard()
        self.ui.setupUi(self)
        self.ui.delete_widget.hide()
        self.ui.cover_widget.lower()
        self.ui.cover_widget.hide()
        self.display_del_btn = False
        self.show_delay = False

        app_data_location = QStandardPaths.writableLocation(QStandardPaths.AppConfigLocation)

        if flag:
            self.icon_dir = os.path.abspath(os.path.join(app_data_location, 'custom'))
        else:
            self.icon_dir = os.path.abspath(os.path.join(app_data_location, 'default'))

        self.ui.delete_btn.clicked.connect(lambda: self.sendDeleteSignal.emit(self.icon_path))

        if title == ':/create.png':
            self.icon_path = title
            self.ui.name.hide()
            self.setCursor(Qt.PointingHandCursor)
        else:
            self.icon_path = os.path.join(self.icon_dir, title)

        self.ui.icon.installEventFilter(self)

        pixmap = QPixmap(self.icon_path)
        pixmap.copy(QRect(0, 0, 130, 68))
        self.ui.icon.setPixmap(pixmap)

        self.ui.name.setText(title.replace('.png', '').replace('.jpg', ''))
        self.ui.using_label.hide()

    def reset_display_del_btn_value(self, flag):
        self.display_del_btn = flag

    def show_using_log(self, flag):
        self.ui.using_label.show() if flag else self.ui.using_label.hide()

    def get_card_name(self):
        return self.icon_path

    def enterEvent(self, event):
        self.sendEnterBackground.emit(self.icon_path)

        if self.icon_path != ':/create.png':
            self.ui.cover_widget.show()
        if self.display_del_btn:
            self.ui.delete_widget.show()

    def leaveEvent(self, event):
        self.sendLeaveBackground.emit()
        if self.icon_path != ':/create.png':
            self.ui.cover_widget.hide()
        if self.display_del_btn:
            self.ui.delete_widget.hide()

    def eventFilter(self, obj, event):
        if obj == self.ui.icon:
            if event.type() == QEvent.MouseButtonRelease:
                if self.icon_path == ':/create.png':
                    self.sendAddSignal.emit()
                else:
                    self.sendBackgroundName.emit(self.icon_path)
        return super().eventFilter(obj, event)
