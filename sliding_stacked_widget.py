from enum import Enum

from PyQt5.QtCore import pyqtSignal, QEasingCurve, QPoint, QPropertyAnimation, QParallelAnimationGroup
from PyQt5.QtWidgets import QStackedWidget


class Direction(Enum):
    LEFT2RIGHT = 0,
    RIGHT2LEFT = 1,
    TOP2BOTTOM = 2,
    BOTTOM2TOP = 3,
    AUTOMATIC = 4


class SlidingStackedWidget(QStackedWidget):
    animationFinished = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)
        if parent is not None:
            self.main_window = parent
        self.vertical = False
        self.speed = 500
        self.animation_type = QEasingCurve.OutBack
        self.now = 0
        self.next = 0
        self.wrap = False
        self.now_point = QPoint(0, 0)
        self.active = False

    def set_speed(self, speed):
        self.speed = speed

    def set_animation(self, animation_type):
        self.animation_type = animation_type

    def set_vertical_mode(self, vertical=True):
        self.vertical = vertical

    def set_wrap(self, wrap):
        self.wrap = wrap

    def slide_in_next(self):
        now = self.currentIndex()
        if self.wrap or (now < self.count() - 1):
            self.slide_in_index(now + 1)

    def slide_in_prev(self):
        now = self.currentIndex()
        if self.wrap or now > 0:
            self.slide_in_index(now - 1)

    def slide_in_index(self, index, direction=Direction.AUTOMATIC):
        if index > self.count() - 1:
            direction = Direction.TOP2BOTTOM if self.vertical else Direction.RIGHT2LEFT
            index = index % self.count()
        elif index < 0:
            direction = Direction.BOTTOM2TOP if self.vertical else Direction.LEFT2RIGHT
            index = (index + self.count()) % self.count()

        self._slide_in_widget(self.widget(index), direction)

    def _slide_in_widget(self, new_widget, direction=Direction.AUTOMATIC):
        if self.active:
            return
        else:
            self.active = True

        now_index = self.currentIndex()
        next_index = self.indexOf(new_widget)
        if now_index == next_index:
            self.active = False
            return
        elif now_index < next_index:
            direction_hint = Direction.TOP2BOTTOM if self.vertical else Direction.RIGHT2LEFT
        else:
            direction_hint = Direction.BOTTOM2TOP if self.vertical else Direction.LEFT2RIGHT

        if direction == Direction.AUTOMATIC:
            direction = direction_hint

        offset_x = self.frameRect().width()
        offset_y = self.frameRect().height()

        self.widget(next_index).setGeometry(0, 0, offset_x, offset_y)

        if direction == Direction.BOTTOM2TOP:
            offset_x = 0
            offset_y = -offset_y
        elif direction == Direction.TOP2BOTTOM:
            offset_x = 0
        elif direction == Direction.RIGHT2LEFT:
            offset_x = -offset_x
            offset_y = 0
        elif direction == Direction.LEFT2RIGHT:
            offset_y = 0

        next_point = self.widget(next_index).pos()
        now_point = self.widget(now_index).pos()
        self.now_point = now_point

        self.widget(next_index).move(next_point.x() - offset_x, next_point.y() - offset_y)
        self.widget(next_index).show()
        self.widget(next_index).raise_()

        self.animation_now = QPropertyAnimation(self.widget(now_index), b'pos')
        self.animation_now.setDuration(self.speed)
        self.animation_now.setEasingCurve(self.animation_type)
        self.animation_now.setStartValue(QPoint(now_point.x(), now_point.y()))
        self.animation_now.setEndValue(QPoint(offset_x + now_point.x(), offset_y + now_point.y()))

        self.animation_next = QPropertyAnimation(self.widget(next_index), b'pos')
        self.animation_next.setDuration(self.speed)
        self.animation_next.setEasingCurve(self.animation_type)
        self.animation_next.setStartValue(QPoint(-offset_x + next_point.x(), offset_y + next_point.y()))
        self.animation_next.setEndValue(QPoint(next_point.x(), next_point.y()))

        self.animation_group = QParallelAnimationGroup()
        self.animation_group.addAnimation(self.animation_now)
        self.animation_group.addAnimation(self.animation_next)

        self.animation_group.finished.connect(self._animation_done_slot)

        self.next = next_index
        self.now = now_index
        self.active = True
        self.animation_group.start()

    def _animation_done_slot(self):
        self.setCurrentIndex(self.next)
        self.widget(self.now).hide()
        self.widget(self.now).move(self.now_point)
        self.active = False
        self.animationFinished.emit()


if __name__ == '__main__':
    import sys
    from PyQt5.QtCore import Qt
    from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QPushButton, QCheckBox, QComboBox, QLabel, \
        QLCDNumber, QSlider, QVBoxLayout, QGridLayout


    class MainWindow(QMainWindow):
        def __init__(self):
            super().__init__()
            self.animation_time = 500
            self.create_gui_control_components()
            self.create_sub_sliding_widgets()
            self.create_sliding_stacked_widget()
            self.create_main_layout()
            self.create_connections()

        def create_gui_control_components(self):
            _min = 500
            _max = 1500
            self.animation_time = (_min + _max) >> 1

            self.button_next = QPushButton('Next')
            self.button_prev = QPushButton('Prev')
            self.check_wrap = QCheckBox('Wrap')
            self.check_vertical = QCheckBox('vertical')

            self.list_all = QComboBox()
            self.list_all.addItem('Page 1')
            self.list_all.addItem('Page 2')
            self.list_all.addItem('Page 3')
            self.list_all.addItem('Page 4')
            self.list_all.setMinimumHeight(40)

            self.speed_label = QLabel('Anim. Time:')
            self.speed_display = QLCDNumber()

            self.slide_speed = QSlider(Qt.Horizontal)
            self.slide_speed.setMinimum(_min)
            self.slide_speed.setMaximum(_max)

            self.slide_speed.setValue(self.animation_time)
            self.speed_display.display(self.animation_time)

        def create_main_layout(self):
            central_widget = QWidget(self)
            main_layout = QVBoxLayout()
            central_widget.setLayout(main_layout)

            control_pane_layout = QGridLayout()
            main_layout.addWidget(self.sliding_stacked)
            main_layout.addLayout(control_pane_layout)
            row = 1
            control_pane_layout.addWidget(self.button_prev, row, 1, 1, 1)
            control_pane_layout.addWidget(self.button_next, row, 2, 1, 1)
            row += 1
            control_pane_layout.addWidget(self.check_wrap, row, 1, 1, 1)
            control_pane_layout.addWidget(self.check_vertical, row, 2, 1, 1)
            row += 1
            control_pane_layout.addWidget(self.speed_label, row, 1, 1, 1)
            control_pane_layout.addWidget(self.speed_display, row, 2, 1, 1)
            row += 1
            control_pane_layout.addWidget(self.slide_speed, row, 1, 1, 2)
            row += 1
            control_pane_layout.addWidget(self.list_all, row, 1, 1, 2)
            self.setCentralWidget(central_widget)

        def create_sub_sliding_widgets(self):
            self.slide_widget1 = QWidget()
            self.slide_widget2 = QWidget()
            self.slide_widget3 = QWidget()
            self.slide_widget4 = QWidget()

            self.slide_widget1_layout = QVBoxLayout()
            self.slide_widget1.setLayout(self.slide_widget1_layout)
            self.slide_widget2_layout = QVBoxLayout()
            self.slide_widget2.setLayout(self.slide_widget2_layout)
            self.slide_widget3_layout = QVBoxLayout()
            self.slide_widget3.setLayout(self.slide_widget3_layout)
            self.slide_widget4_layout = QVBoxLayout()
            self.slide_widget4.setLayout(self.slide_widget4_layout)

            b11 = QPushButton('Qt')
            self.slide_widget1_layout.addWidget(b11)
            b12 = QPushButton('is cool !')
            self.slide_widget1_layout.addWidget(b12)

            b21 = QPushButton('Cool')
            self.slide_widget2_layout.addWidget(b21)
            b22 = QPushButton('is Qt !')
            self.slide_widget2_layout.addWidget(b22)

            b31 = QPushButton("Isn't")
            self.slide_widget3_layout.addWidget(b31)
            b32 = QPushButton('Qt cool ?')
            self.slide_widget3_layout.addWidget(b32)

            b41 = QPushButton('How cool')
            self.slide_widget4_layout.addWidget(b41)
            b42 = QPushButton('is Qt !')
            self.slide_widget4_layout.addWidget(b42)

        def create_sliding_stacked_widget(self):
            self.sliding_stacked = SlidingStackedWidget(self)
            self.sliding_stacked.addWidget(self.slide_widget1)
            self.sliding_stacked.addWidget(self.slide_widget2)
            self.sliding_stacked.addWidget(self.slide_widget3)
            self.sliding_stacked.addWidget(self.slide_widget4)
            self.sliding_stacked.set_speed(self.animation_time)

        def create_connections(self):
            self.button_next.pressed.connect(self.sliding_stacked.slide_in_next)
            self.button_prev.pressed.connect(self.sliding_stacked.slide_in_prev)
            self.check_wrap.clicked.connect(self.sliding_stacked.set_wrap)
            self.check_vertical.clicked.connect(self.sliding_stacked.set_vertical_mode)
            self.slide_speed.valueChanged.connect(self.sliding_stacked.set_speed)
            self.slide_speed.valueChanged.connect(self.speed_display.display)
            self.list_all.currentIndexChanged.connect(self.sliding_stacked.slide_in_index)


    app = QApplication(sys.argv)
    main_window = MainWindow()
    main_window.resize(360, 504)
    main_window.show()
    sys.exit(app.exec_())
