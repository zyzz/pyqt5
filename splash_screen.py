import time

from PyQt5.QtWidgets import QApplication, QSplashScreen
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap


class SplashScreen(QSplashScreen):
    def __init__(self, t, splash_image):
        if not isinstance(splash_image, QPixmap):
            image = QPixmap(splash_image)
        else:
            image = splash_image
        super(SplashScreen, self).__init__(image)  # 启动程序的图片
        self.setWindowModality(Qt.ApplicationModal)
        self.fade_ticker(t)
        QApplication.instance().processEvents()

    def fade_ticker(self, keep_t):
        self.setWindowOpacity(0)
        t = 0
        while t <= 50:
            new_ppacity = self.windowOpacity() + 0.02  # 设置淡入
            if new_ppacity > 1:
                break
            self.setWindowOpacity(new_ppacity)
            self.show()
            t -= 1
            time.sleep(0.04)
        self.show()
        time.sleep(keep_t)
        t = 0
        while t <= 50:
            new_ppacity = self.windowOpacity() - 0.02  # 设置淡出
            if new_ppacity < 0:
                self.close()
                break
            self.setWindowOpacity(new_ppacity)
            self.show()
            t += 1
            time.sleep(0.04)
