import os

from PyQt5.QtCore import QSettings, QStandardPaths, QDir, QPropertyAnimation, QPoint, QParallelAnimationGroup, \
    QFileInfo, QThread, QTimer
from PyQt5.QtWidgets import qApp, QWidget
from PyQt5.QtGui import QPixmap, QPalette, QBrush

import utils
from resources.ui.ui_main_window import Ui_main_window
from draggable_shadow_widget import DraggableShadowWidget
from skin_center import SkinCenter
from home_page import HomePage
from app_page import AppPage
from monitor_page import MonitorPage
from settings_page import SettingsPage
from utilities_page import UtilitiesPage
from monitor_worker import MonitorWorker

from task import Task


class MainWindow(DraggableShadowWidget, Ui_main_window):
    def __init__(self):
        super().__init__()
        self.setFixedSize(910, 610)

        self.background_widget = QWidget(self)
        self.background_widget.setGeometry(0 + utils.SHADOW_WIDTH, 0 + utils.SHADOW_WIDTH, 900, 186)
        self.background_widget.setAutoFillBackground(True)

        self.setupUi(self)

        self.main_layout.setContentsMargins(utils.SHADOW_WIDTH, utils.SHADOW_WIDTH, utils.SHADOW_WIDTH,
                                            utils.SHADOW_WIDTH)
        self.setWindowOpacity(1)

        self.version = 'V1.0.0'

        self.settings = QSettings(utils.COMPANY_SETTING, utils.SETTING_FILE_NAME_SETTING)
        self.settings.setIniCodec('UTF-8')

        self.settings.beginGroup('Background')
        self.last_skin_path = self.settings.value('Path')
        if not self.last_skin_path:
            self.last_skin_path = ':/background/skin/1.png'
            self.settings.setValue('Path', self.last_skin_path)
        else:
            skin_list = self.filter_skin()
            try:
                next(skin for skin in skin_list if skin == self.last_skin_path)
            except StopIteration:
                self.last_skin_path = skin_list[0]
                self.settings.setValue('Path', self.last_skin_path)
        self.settings.endGroup()
        self.settings.sync()
        self.main_skin_pixmap = QPixmap(self.last_skin_path)
        self.review_skin_pixmap = QPixmap()

        palette_back = QPalette()
        palette_back.setBrush(QPalette.Background, QBrush(QPixmap(self.last_skin_path)))
        self.background_widget.setPalette(palette_back)

        self.home_page = HomePage(self)
        self.app_page = AppPage(self)
        self.monitor_page = MonitorPage(self)
        self.settings_page = SettingsPage(self)
        self.utilities_page = UtilitiesPage(self)

        custom_widgets = [
            self.title_widget,
            self.tool_widget,
            self.home_page,
            self.app_page,
            self.monitor_page,
            self.settings_page,
            self.utilities_page
        ]

        for widget in custom_widgets:
            widget.init_ui()
            widget.init_connect()

        self.close_animation = QPropertyAnimation(self, b"windowOpacity")
        self.close_animation.setDuration(500)
        self.close_animation.setStartValue(1.0)
        self.close_animation.setEndValue(0.0)

        self.skin_center = None
        self.login_widget = None

        # self.monitor_worker = MonitorWorker()
        # self.monitor_worker.stateChanged.connect(lambda state: print(state))

        self.task_instance = Task(self)
        '''
        self.monitor_thread = QThread()
        self.timer = QTimer()
        
        

        self.timer.timeout.connect(self.monitor_worker.on_timeout)
        self.timer.start(1000)

        self.monitor_worker.moveToThread(self.monitor_thread)

        self.monitor_thread.start()
        '''

    def open_skin_center(self):
        if self.skin_center is None:
            self.skin_center = SkinCenter(self.last_skin_path)
            self.skin_center.set_parent_window(self)
            self.skin_center.init_sys_background_list()
        w_x = self.frameGeometry().topLeft().x() + (900 / 2) - (442 / 2)
        w_y = self.frameGeometry().topLeft().y() + (600 / 2) - (340 / 2)
        self.skin_center.move(w_x, w_y)
        self.skin_center.show()
        self.skin_center.raise_()

    def get_current_background_index(self):
        index = 1
        self.settings.beginGroup('Background')
        cur_skin_path = self.settings.value('Path')
        file_info = QFileInfo(cur_skin_path)
        if file_info.isFile():
            index = int(file_info.baseName())
        self.settings.endGroup()
        self.settings.sync()
        return index

    def get_current_background_name(self):
        self.settings.beginGroup('Background')
        cur_skin_path = self.settings.value('Path')
        file_info = QFileInfo(cur_skin_path)
        if file_info.isFile():
            cur_skin_path = file_info.fileName()
        self.settings.endGroup()
        self.settings.sync()
        return cur_skin_path

    def get_current_background_abs_name(self):
        self.settings.beginGroup('Background')
        cur_skin_path = self.settings.value('Path')
        self.settings.endGroup()
        self.settings.sync()
        return cur_skin_path

    def restore_skin(self):
        self.main_skin_pixmap.detach()

        self.main_skin_pixmap.load(self.last_skin_path)
        palette_back = QPalette()
        palette_back.setBrush(QPalette.Background, QBrush(self.main_skin_pixmap))
        self.background_widget.setPalette(palette_back)

    def change_skin(self, picture):
        self.main_skin_pixmap.detach()

        self.last_skin_path = picture
        self.main_skin_pixmap.load(picture)
        palette = QPalette()
        palette.setBrush(QPalette.Background, QBrush(self.main_skin_pixmap))
        self.background_widget.setPalette(palette)

        self.settings.beginGroup('Background')
        self.settings.setValue('Path', picture)
        self.settings.endGroup()
        self.settings.sync()

        if self.login_widget is not None:
            self.login_widget.reset_skin(self.last_skin_path)

    def review_the_point_skin(self, picture):
        self.review_skin_pixmap.detach()

        self.review_skin_pixmap.load(picture)
        palette_back = QPalette()
        palette_back.setBrush(QPalette.Background, QBrush(self.review_skin_pixmap))
        self.background_widget.setPalette(palette_back)

    def review_the_org_skin(self):
        self.review_skin_pixmap.detach()

        self.review_skin_pixmap.load(self.last_skin_path)
        palette_back = QPalette()
        palette_back.setBrush(QPalette.Background, QBrush(self.review_skin_pixmap))
        self.background_widget.setPalette(palette_back)

    def init_animation(self):
        shadow_point = QPoint(utils.SHADOW_WIDTH, utils.SHADOW_WIDTH)

        origin_point = QPoint(0, 227) + shadow_point
        need_point = QPoint(0, 150) + shadow_point

        origin_point1 = QPoint(0, 0) + shadow_point
        need_point1 = QPoint(0 + utils.SHADOW_WIDTH, -227 - utils.SHADOW_WIDTH)

        origin_point2 = QPoint(0 + utils.SHADOW_WIDTH, -150 - utils.SHADOW_WIDTH)
        need_point2 = QPoint(0, 0) + shadow_point

        origin_point3 = QPoint(0, 274) + shadow_point
        need_point3 = QPoint(0 + utils.SHADOW_WIDTH, 600 + utils.SHADOW_WIDTH * 2)

        origin_point4 = QPoint(0 + utils.SHADOW_WIDTH, 600 + utils.SHADOW_WIDTH * 2)
        need_point4 = QPoint(0, 197) + shadow_point

        self.main_action_animation = QPropertyAnimation(self.default_action_widget, b'pos')
        self.main_action_animation.setDuration(500)
        self.main_action_animation.setStartValue(origin_point1)
        self.main_action_animation.setEndValue(need_point1)

        self.main_action_animation2 = QPropertyAnimation(self.other_action_widget, b'pos')
        self.main_action_animation2.setDuration(500)
        self.main_action_animation2.setStartValue(origin_point2)
        self.main_action_animation2.setEndValue(need_point2)

        self.main_tool_animation = QPropertyAnimation(self.tool_widget, b'pos')
        self.main_tool_animation.setDuration(500)
        self.main_tool_animation.setStartValue(origin_point)
        self.main_tool_animation.setEndValue(need_point)

        self.main_content_animation = QPropertyAnimation(self.default_content_widget, b'pos')
        self.main_content_animation.setDuration(500)
        self.main_content_animation.setStartValue(origin_point3)
        self.main_content_animation.setEndValue(need_point3)

        self.main_content_animation2 = QPropertyAnimation(self.other_content_widget, b'pos')
        self.main_content_animation2.setDuration(500)
        self.main_content_animation2.setStartValue(origin_point4)
        self.main_content_animation2.setEndValue(need_point4)

        self.spread_group = QParallelAnimationGroup(self)
        self.spread_group.addAnimation(self.main_action_animation)
        self.spread_group.addAnimation(self.main_action_animation2)
        self.spread_group.addAnimation(self.main_tool_animation)
        self.spread_group.addAnimation(self.main_content_animation)
        self.spread_group.addAnimation(self.main_content_animation2)

        self.main_action_back_animation2 = QPropertyAnimation(self.other_action_widget, b'pos')
        self.main_action_back_animation2.setDuration(500)
        self.main_action_back_animation2.setStartValue(need_point2)
        self.main_action_back_animation2.setEndValue(origin_point2)

        self.main_action_back_animation = QPropertyAnimation(self.default_action_widget, b'pos')
        self.main_action_back_animation.setDuration(500)
        self.main_action_back_animation.setStartValue(need_point1)
        self.main_action_back_animation.setEndValue(origin_point1)

        self.main_tool_back_animation = QPropertyAnimation(self.tool_widget, b'pos')
        self.main_tool_back_animation.setDuration(500)
        self.main_tool_back_animation.setStartValue(need_point)
        self.main_tool_back_animation.setEndValue(origin_point)

        self.main_content_back_animation = QPropertyAnimation(self.default_content_widget, b'pos')
        self.main_content_back_animation.setDuration(500)
        self.main_content_back_animation.setStartValue(need_point3)
        self.main_content_back_animation.setEndValue(origin_point3)

        self.main_content_back_animation2 = QPropertyAnimation(self.other_content_widget, b'pos')
        self.main_content_back_animation2.setDuration(500)
        self.main_content_back_animation2.setStartValue(need_point4)
        self.main_content_back_animation2.setEndValue(origin_point4)

        self.gather_group = QParallelAnimationGroup(self)
        self.gather_group.addAnimation(self.main_action_back_animation)
        self.gather_group.addAnimation(self.main_action_back_animation2)
        self.gather_group.addAnimation(self.main_tool_back_animation)
        self.gather_group.addAnimation(self.main_content_back_animation)
        self.gather_group.addAnimation(self.main_content_back_animation2)

        self.spread_group.finished.connect(self.up_animation_finished)
        self.gather_group.finished.connect(self.close_animation_finished)

    def up_animation_finished(self):
        self.tool_widget.show()
        if self.title_widget.isHidden():
            self.title_widget.show()
        self.login_widget.hide()
        self.enable_shadow(True)

    def close_animation_finished(self):
        self.tool_widget.show()
        if self.title_widget.isHidden():
            self.title_widget.show()

        self.login_widget.show()
        self.enable_shadow(True)

    def close(self):
        # self.monitor_thread.terminate()
        self.close_animation.start()
        self.close_animation.finished.connect(super().close)

    @staticmethod
    def get_app_config_location():
        return QStandardPaths.writableLocation(QStandardPaths.AppConfigLocation)

    @staticmethod
    def filter_skin():
        app_data_location = QStandardPaths.writableLocation(QStandardPaths.AppConfigLocation)
        path = os.path.abspath(os.path.join(app_data_location, 'default'))
        path2 = os.path.abspath(os.path.join(app_data_location, 'custom'))
        pic_dir = QDir(path)
        pic_dir.setFilter(QDir.Files | QDir.Hidden | QDir.NoSymLinks)
        pic_dir.setSorting(QDir.Size | QDir.Reversed)
        pic_dir2 = QDir(path2)
        pic_dir2.setFilter(QDir.Files | QDir.Hidden | QDir.NoSymLinks)
        pic_dir2.setSorting(QDir.Size | QDir.Reversed)
        filters = ['*.jpg', '*.png']
        pic_dir.setNameFilters(filters)
        pic_dir2.setNameFilters(filters)
        entry_info_list = pic_dir.entryInfoList()
        entry_info_list2 = pic_dir2.entryInfoList()

        skin_list = []
        if len(entry_info_list) < 1 and len(entry_info_list2) < 1:
            skin_list.append(':/background/skin/1.png')
            skin_list.append(':/background/skin/2.png')
            skin_list.append(':/background/skin/3.png')
            skin_list.append(':/background/skin/4.png')
        elif len(entry_info_list) > 0 and len(entry_info_list2) > 0:

            for file_info in entry_info_list:
                skin_list.append(os.path.join(path, file_info.fileName()))
            for file_info in entry_info_list2:
                skin_list.append(os.path.join(path2, file_info.fileName()))
            skin_list.append(':/background/skin/1.png')
            skin_list.append(':/background/skin/2.png')
            skin_list.append(':/background/skin/3.png')
            skin_list.append(':/background/skin/4.png')
        else:
            if len(entry_info_list) > 0:
                for file_info in entry_info_list:
                    skin_list.append(os.path.join(path, file_info.fileName()))
                skin_list.append(':/background/skin/1.png')
                skin_list.append(':/background/skin/2.png')
                skin_list.append(':/background/skin/3.png')
                skin_list.append(':/background/skin/4.png')
            elif len(entry_info_list2) > 0:
                for file_info in entry_info_list2:
                    skin_list.append(os.path.join(path2, file_info.fileName()))
                skin_list.append(':/background/skin/1.png')
                skin_list.append(':/background/skin/2.png')
                skin_list.append(':/background/skin/3.png')
                skin_list.append(':/background/skin/4.png')
        return skin_list

    def closeEvent(self, event):
        qApp.exit()

    def hide_widget(self):
        self.enable_shadow(False)
        self.tool_widget.hide()
        if self.title_widget.isVisible():
            self.title_widget.hide()
        if self.login_widget.isVisible():
            self.login_widget.hide()

    def set_current_page_index(self, index):
        self.stacked_widget.slide_in_index(index)


if __name__ == '__main__':
    import sys
    from PyQt5.QtWidgets import QApplication

    app = QApplication(sys.argv)
    try:
        main_window = MainWindow()
        main_window.show()
    except Exception as e:
        print(e)
    sys.exit(app.exec_())
