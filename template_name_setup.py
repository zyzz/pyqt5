from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QDialog, QLabel, QVBoxLayout, QHBoxLayout, QSpacerItem, QSizePolicy, QLineEdit,
                             QDialogButtonBox, QMessageBox)

from models import Session, ImportProfile


class TemplateNameSetup(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setFixedSize(400, 95)
        self.setWindowTitle("模板")
        self.verticalLayout = QVBoxLayout(self)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QLabel(self)
        self.label.setObjectName("label")
        self.label.setText("请输入模板名称")
        self.horizontalLayout.addWidget(self.label)
        spacer_item = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacer_item)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.line_edit = QLineEdit(self)
        self.line_edit.setObjectName("line_edit")
        self.verticalLayout.addWidget(self.line_edit)
        self.buttonBox = QDialogButtonBox(self)
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel | QDialogButtonBox.Ok)
        self.buttonBox.setCenterButtons(True)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

    def accept(self):
        template_name = self.line_edit.text()
        if not template_name:
            QMessageBox.information(None, '提示', '模板名称不能为空！')
            self.line_edit.setFocus()
            return
        session = Session()
        if session.query(ImportProfile).filter_by(name=template_name).first() is not None:
            QMessageBox.information(None, '提示', '模板名称已存在！')
            self.line_edit.setFocus()
            session.close()
            return
        session.close()
        super().accept()

    def get_template_name(self):
        return self.line_edit.text()


if __name__ == '__main__':
    import sys
    from PyQt5.QtWidgets import QApplication

    app = QApplication(sys.argv)
    dialog = TemplateNameSetup()
    dialog.exec_()
