from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QToolButton
from PyQt5.QtGui import QPixmap, QIcon


class ToolButton(QToolButton):
    def __init__(self, parent):
        super().__init__(parent)
        self.setFocusPolicy(Qt.NoFocus)

        self.setToolButtonStyle(Qt.ToolButtonTextBesideIcon)
        self.setPopupMode(QToolButton.InstantPopup)
        self.setObjectName("transparentToolButton")
        self.pressed = False

    def mousePressEvent(self, event):
        if self.isEnabled() and event.button() == Qt.LeftButton:
            pass  # kobe: don't emit, it will emit by click it self.
        QToolButton.mousePressEvent(event)

    '''
    def set_mouse_press(self, is_press):
        self.mouse_press = is_press

        if self.mouse_press:
            self.setIcon(self.press_icon)
            self.pressed = True
        else:
            self.setIcon(self.normal_icon)
            self.pressed = False
    '''