# 数据采集中心

![pyversions](https://img.shields.io/badge/python%20-3.5%2B-blue.svg) ![celery](https://img.shields.io/badge/celery-3.1.25-4BC51D.svg) ![pyqt5](https://img.shields.io/badge/pyqt5-5.9.2-orange.svg)

数据采集中心采用了典型的C/S架构，由C端产生任务放置消息队列，有S端来统一处理任务并反馈结果。任务包括通过模板配置应用，通过不同的参数和策略来收集数据并上传至云平台。

## 预览
![效果图](https://gitlab.com/zyzz/pyqt5/raw/master/intro/1.png)
![登陆](https://gitlab.com/zyzz/pyqt5/raw/master/intro/2.png)

## 特性
- **多平台**： 支持`windows`、`macos`、`linux`操作系统
- **灵活配置**： 通过相当可配置`txt`、`csv`、`xls`等格式的本地文件，后续将支持`mysql`、`mssql`、`postgrey`、`oracle`等关系型数据库
- **配置同步**：用的配置目前通过`sqlite3`进行存储，因为使用了`orm`框架，可无缝结合到`mysql`、`mssql`、`postgrey`、`oracle`等关系型数据库
- **分布式**： 采用消息队列的机制，轻松横向扩展。
- **实时监控**：能够显示任务的详细信息。
- **远程控制**：控制进程池大小，平滑配置（autoscale settings），应用时间和速率限制。
- **断点续传**： 利用`redis`对每条记录的md5值进行缓存，可有效避免数据重复采集。
- **界面友好**：`ui`部分采用`pyqt5`来实现，可动态换肤、自定义皮肤。更简洁易用的界面。


## 本地开发

首先确认本机已经安装 [Python](http://python.org/) 运行环境。

### 开发规范
[编码风格规范](http://zh-google-styleguide.readthedocs.io/en/latest/google-python-styleguide/python_style_rules/)  
格式请遵守pep8,务必开启编辑器的pylint和pep8检测

### 安装依赖：

``` bash
pip install -r requirements.txt
```


### 启动项目：

``` bash
(venv) python main.py
```

## 部署

将程序进行打包：
``` bash
(venv) python setup.py build
```


## 相关文档

* [PyQt5 Reference Guide](http://pyqt.sourceforge.net/Docs/PyQt5/)
* [sqlalchemy](http://www.sqlalchemy.org/)
* [celery](http://docs.celeryproject.org/en/3.1/)
* [flower](http://flower.readthedocs.io/en/latest/)
