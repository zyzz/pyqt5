import sys
from cx_Freeze import setup, Executable


# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
    pass
    # base = 'Win32GUI'
'''
options = {
    'build_exe': {
        'namespace_packages': ['celery']
    }
}

executables = [
    Executable('app.py', base=base)
]

setup(name="hello",
      version="0.1",
      description="my application!",
      options=options,
      executables=executables)
'''

'''
options = {
    'build_exe': {
        'namespace_packages': ['json', 'celery', 'celery.fixups.django', 'celery.bin.worker', 'celery.loaders.app',
                               'celery_app.celeryconfig', 'celery.concurrency.prefork', 'celery.apps.worker',
                               'celery_app.tasks', 'celery.app.log']
    }
}
'''

options = {
    'build_exe': {
        'packages': ['celery', 'billiard', 'celery_app']
    }
}


executables = [
    Executable('app.py', base=None)
]

setup(name="hello",
      version="0.1",
      description="my application!",
      options=options,
      executables=executables)
