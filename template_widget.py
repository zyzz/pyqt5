import cgi
from collections import OrderedDict

from PyQt5.QtCore import Qt, QSize
from PyQt5.QtWidgets import QWidget, QListWidget, QListWidgetItem, QListView, QSplitter, QSizePolicy, QStackedWidget, \
    QVBoxLayout, QHBoxLayout
from PyQt5.QtGui import QPalette, QBrush

# from resources.ui import templete_widget
from models import Session, ImportProfile
from scroll_widget import ScrollWidget
from template_page import TemplatePage


class TemplateWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setFixedSize(900, 403)
        self.setAutoFillBackground(True)

        palette = QPalette()
        palette.setBrush(QPalette.Window, QBrush(Qt.white))
        self.setPalette(palette)

        self.splitter = QSplitter(self)
        self.splitter.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.splitter.setOrientation(Qt.Horizontal)
        self.splitter.setHandleWidth(1)

        self.template_widget = QListWidget()
        self.template_widget.setFixedSize(150, 403)
        self.template_widget.setFocusPolicy(Qt.NoFocus)
        self.template_widget.setObjectName('infoList')

        self.template_widget.setIconSize(QSize(16, 16))
        self.template_widget.setResizeMode(QListView.Adjust)
        self.template_widget.setViewMode(QListView.ListMode)
        self.template_widget.setMovement(QListView.Static)

        self.stacked_widget = QStackedWidget(self)
        self.stacked_widget.setFocusPolicy(Qt.NoFocus)
        self.stacked_widget.setAutoFillBackground(True)

        self.template_widget.currentRowChanged.connect(self.change_info_page)

        center_layout = QVBoxLayout()
        center_layout.addWidget(self.stacked_widget)
        center_layout.setSpacing(0)
        center_layout.setContentsMargins(0, 0, 0, 0)
        self.splitter.addWidget(self.template_widget)
        self.splitter.addWidget(self.stacked_widget)

        for i in range(self.splitter.count()):
            handle = self.splitter.handle(i)
            handle.setEnabled(False)

        main_layout = QHBoxLayout()
        main_layout.addWidget(self.splitter)
        main_layout.setSpacing(0)
        main_layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(main_layout)

        session = Session()
        for profile in session.query(ImportProfile).all():
            self.on_add_template(profile.name)
        session.close()

        if self.template_widget.count() > 0:
            self.template_widget.setCurrentRow(0)

    def change_info_page(self, current_row):
        self.stacked_widget.setCurrentWidget(self.stacked_widget.widget(current_row))

    def on_add_template(self, name):
        session = Session()

        profile = session.query(ImportProfile).filter_by(name=name).one()
        '''
        
        item = QListWidgetItem(profile.name)
        item.setSizeHint(QSize(120, 36))
        # item.setStatusTip()
        self.template_widget.addItem(item)
        '''

        scroll_widget = ScrollWidget()
        template_page = TemplatePage('导入格式', scroll_widget.zone)
        template_page.set_map({"导入类型": profile.import_type})
        template_page.init_ui()
        template_page.show()
        scroll_widget.add_scroll_widget(template_page)

        template_page = TemplatePage('数据源', scroll_widget.zone)
        items = OrderedDict()
        items['文件位置'] = profile.source_file
        items['文件编码'] = profile.source_code_page
        template_page.set_map(items)
        template_page.init_ui()
        template_page.show()
        scroll_widget.add_scroll_widget(template_page)

        template_page = TemplatePage('分隔符', scroll_widget.zone)
        items = OrderedDict()
        items['记录分隔符'] = profile.row_delimiter
        items['字段分隔符'] = self._get_field_delimiter(profile.field_delimiter)
        items['文本限定符'] = cgi.html.unescape(profile.text_qualifier)
        template_page.set_map(items)
        template_page.init_ui()
        template_page.show()

        scroll_widget.add_scroll_widget(template_page)

        template_page = TemplatePage('附加选项', scroll_widget.zone)
        items = OrderedDict()
        items['字段名行'] = profile.field_name_row
        items['第一个数据行'] = profile.first_row
        items['最后一个数据行'] = profile.last_row
        template_page.set_map(items)
        template_page.init_ui()
        template_page.show()
        scroll_widget.add_scroll_widget(template_page)

        template_page = TemplatePage('导入模式', scroll_widget.zone)
        items = OrderedDict()
        items['模式'] = profile.import_mode
        template_page.set_map(items)
        template_page.init_ui()
        template_page.show()
        scroll_widget.add_scroll_widget(template_page)

        self.stacked_widget.addWidget(scroll_widget)

        session.close()

    @staticmethod
    def _get_field_delimiter(delimiter):
        if delimiter == '0x9':
            return '定位'
        elif delimiter == '0x3b':
            return '分号'
        elif delimiter == '0x2c':
            return '逗号'
        elif delimiter == '0x20':
            return '空格'
        elif delimiter == '':
            return '无'
        else:
            return delimiter
